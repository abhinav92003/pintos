#ifndef __LIB_KERNEL_FIXED_POINT_H
#define __LIB_KERNEL_FIXED_POINT_H
#include <stdint.h> 

typedef int32_t fixed_point;
fixed_point convert_int_to_fp (int, int);
int convert_fp_to_int_truncate (fixed_point, int);
int convert_fp_to_int_nearest (fixed_point, int);
fixed_point add_fp_fp (fixed_point, fixed_point);
fixed_point add_fp_int (fixed_point, int, int);
fixed_point subtract_fp_fp (fixed_point, fixed_point);
fixed_point subtract_fp_int (fixed_point, int, int);
fixed_point multiply_fp_fp (fixed_point, fixed_point, int);
fixed_point multiply_fp_int (fixed_point, int);
fixed_point divide_fp_fp (fixed_point, fixed_point, int);
fixed_point divide_fp_int (fixed_point, int);

#endif /* lib/kernel/fixed_point.h */
