#include "fixed_point.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

/* Convert an integer n to (31-q).q fixed point format */
fixed_point 
convert_int_to_fp (int n, int q)
{
  ASSERT (q <= 31);
  return (n << q);
}

/* Convert a number x represented in (31-q).q fixed point format to its 
   integer equivalent. The number is rounded down */
int 
convert_fp_to_int_truncate (fixed_point x, int q)
{
  ASSERT (q <= 31);
  return (x >> q);
}

/* Convert a number x represented in (31-q).q fixed point format to its 
   integer equivalent. The number is rounded to the nearest integer */
int 
convert_fp_to_int_nearest (fixed_point x, int q)
{
  ASSERT (q <= 31);
  int f = (1 << q);
  if (x >= 0)
    return (x + f / 2) / f;
  else
    return (x - f / 2) / f;
}

/* Add two numbers that are represented in fixed point format */
fixed_point 
add_fp_fp (fixed_point x, fixed_point y)
{
  return x + y;  
}

/* Add a number x represented in (31-q).q fixed point format and an integer n */ 
fixed_point 
add_fp_int (fixed_point x, int n, int q)
{
  ASSERT (q <= 31);
  return x + (n << q);
}

/* Subtract two numbers that are represented in fixed point format */
fixed_point 
subtract_fp_fp (fixed_point x, fixed_point y)
{
  return x - y;
}

/* Subtract an integer n from a number x represented in (31-q).q fixed point format */ 
fixed_point 
subtract_fp_int (fixed_point x, int n, int q)
{
  ASSERT (q <= 31);
  return x - (n << q);
}

/* Multiply two numbers x,y that are represented in (31-q).q fixed point format */
fixed_point 
multiply_fp_fp (fixed_point x, fixed_point y, int q)
{
  ASSERT (q <= 31);
  return (((int64_t) x) * y) / (1 << q);
}

/* Multiply a number x represented in fixed point format and an integer n */
fixed_point 
multiply_fp_int (fixed_point x, int n)
{
  return x * n;
}

/* Divide two numbers x,y that are represented in (31-q).q fixed point format */
fixed_point 
divide_fp_fp (fixed_point x, fixed_point y, int q)
{
  ASSERT (q <= 31);
  return (((int64_t) x) * (1 << q)) / y;
}

/* Divide a number x represented in fixed point format and an integer n */
fixed_point 
divide_fp_int (fixed_point x, int n)
{
  return x / n;
}
