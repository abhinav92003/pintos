#include "userprog/syscall.h"
#include "devices/shutdown.h"
#include "threads/malloc.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "userprog/process.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "devices/input.h"
#include "lib/string.h"
#include "vm/page_frame.h"
#include "filesys/free-map.h"
#include "filesys/inode.h"

#define READDIR_MAX_LEN NAME_MAX

static void syscall_handler (struct intr_frame *);

/* Functions that handle the specific system call.
   Invoked by syscall_handler(), depending on system call number */
static bool create (const char *, unsigned);
static bool remove (const char *);
static off_t read (int, void *, unsigned);
static off_t write (int, void *, unsigned);
static int open (const char *);
static void seek (int fd, unsigned position);
static unsigned tell (int fd);
static int filesize (int fd); 

static mapid_t mmap (int, void*);


static uint32_t stack_pop (void **);

static char *copy_string_malloc (char *);
static void *copy_data_n_malloc (void *, int);

int is_dir (int);
block_sector_t get_inumber (int);
static int chdir (char *);
static int mkdir (char *);
static int readdir (int fd, char *buffer);

void remove_end_slash (char *);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

/* Copy string s to a new memory location allocated using malloc */
char *copy_string_malloc (char *s)
{
  int len = is_string_addr_valid (s);
  return copy_data_n_malloc (s, len + 1);
}

/* Copy n bytes of data from s to a new memory location 
   allocated using malloc */
void* copy_data_n_malloc (void *s, int n)
{
  is_addr_valid_n (s, n);
  void *s2 = allocate_mem_malloc (n);
  memcpy (s2, s, n);
  return s2;
}
/* Check if addr is a valid mapped user address. If not then exit */
void is_addr_valid (void *addr)
{
  if (!(addr != NULL && addr < PHYS_BASE ))
    //  && pagedir_get_page (thread_current ()->pagedir, addr) != NULL))
    exit (-1);
/*  uintptr_t pgno = pg_no (addr);
  struct list_elem *e;
  for (e = list_begin (&thread_current ()->spt); e != list_end (
          &thread_current ()->spt); e = list_next (e))
    {
      if (pg_no(list_entry (e, struct spte, elem)->page) == pgno)
        break;
    }
  if (e == list_end (&thread_current ()->spt))
    exit (-1);*/
}

/* Check if first n addresses starting from address s are valid */
void is_addr_valid_n (void *s, int n)
{
  int i = 0;
  for (i = 0; i < n; i += PGSIZE)
      is_addr_valid (s + i);
  if (s + i - PGSIZE != s + n - 1)
   is_addr_valid (s + n - 1);
}

/* Check if all characters of string starting at address s are located at
   a valid address
   If all addresses are valid, returns the length of string */
int is_string_addr_valid (char *s)
{
  int len = 0;
  while (1)
    {
      is_addr_valid (s);
      if (*s == '\0')
        break;
      len++;
      s++;
    }
  return len;
}


/* Pop 4 bytes from top of stack */
uint32_t stack_pop (void **stack)
{
  is_addr_valid (*stack);
  is_addr_valid (*stack + sizeof (uint32_t) - 1);
  uint32_t retval = *(uint32_t *)(*stack);
  (*stack) += sizeof (uint32_t);
  return retval;
}

void remove_end_slash (char *str)
{
  int len = strlen (str);
  len--;
  while (len >= 0 && str [len] == '/')
    len--;
  len++;
  if (len != 0) 
    str [len] = '\0';
  /* If str is all slashes only, then no change is made */
}

static void
syscall_handler (struct intr_frame *f) 
{ 
  void *stack = f->esp;
  int32_t int_type = (int32_t)stack_pop (&stack);
  int retval = 0;
  int fd, size, exit_code,id;
  char *file;
  void *buffer, *buffer2, *addr;
  unsigned initial_size, pos;
  mapid_t mapid;
  switch (int_type)
    {
      case SYS_EXEC:
        file = (char *)stack_pop (&stack);
        file = copy_string_malloc (file); 
        retval = process_execute (file);   
        free_mem_malloc (file);       
        if (retval == TID_ERROR)
          retval = -1;
        break;
        
      case SYS_HALT:
        shutdown_power_off ();
        break;
        
      case SYS_WAIT:
        id = (int)stack_pop (&stack);
        retval = process_wait (id);
        break;
        
      case SYS_EXIT: 
        exit_code = (int)stack_pop (&stack);
        exit (exit_code);       
        break;
        
      case SYS_REMOVE:
        file = (char *)stack_pop (&stack);
        file = copy_string_malloc (file);
        remove_end_slash (file); 
        retval = remove (file);  
        free_mem_malloc (file);
        break;
        
      case SYS_READ:
        fd = (int)stack_pop (&stack);
        buffer = (void *)stack_pop (&stack);
        size = (int)stack_pop (&stack);
        is_addr_valid_n (buffer, size);
        buffer2 = allocate_mem_malloc (size); 
        retval = read (fd, buffer2, size);  
        memcpy (buffer, buffer2, size);
        free_mem_malloc (buffer2);
        break;
        
      case SYS_FILESIZE:
        fd = (int)stack_pop (&stack); 
        retval = filesize (fd);  
        break;
        
      case SYS_SEEK:
        fd = (int)stack_pop (&stack);
        pos = (int)stack_pop (&stack); 
        seek (fd, pos);  
        break;
        
      case SYS_TELL:
        fd = (int)stack_pop (&stack); 
        retval = tell (fd);  
        break;
        
      case SYS_CREATE:
        file = (char *)stack_pop (&stack);
        initial_size = (int)stack_pop (&stack);
        file = copy_string_malloc (file); 
        retval = create (file, initial_size);  
        free_mem_malloc (file);
        break;
        
      case SYS_OPEN:    
        file = (char *)stack_pop (&stack);
        file = copy_string_malloc (file);
        remove_end_slash (file); 
        retval = open (file);  
        free_mem_malloc (file);
        break;
        
      case SYS_CLOSE:
        fd = (int)stack_pop (&stack); 
        close (fd);  
        break;
        
      case SYS_WRITE:
        fd = (int)stack_pop (&stack);
        buffer = (char *)stack_pop (&stack);
        size = (int)stack_pop (&stack);
        buffer2 = copy_data_n_malloc (buffer, size); 
        retval = write (fd, buffer2, size);  
        free_mem_malloc (buffer2);
        break;
      
      case SYS_MMAP:
        fd = (int)stack_pop (&stack);
        addr = (void *)stack_pop (&stack);
        retval = mmap (fd, addr);
        break;
        
      case SYS_MUNMAP:
        mapid = (mapid_t)stack_pop (&stack);
        munmap (mapid);
        break;
      
      case SYS_CHDIR:
        file = (char *)stack_pop (&stack);
        file = copy_string_malloc (file);
        remove_end_slash (file); 
        retval = chdir (file);  
        free_mem_malloc (file);
        break;
      
      case SYS_MKDIR:
        file = (char *)stack_pop (&stack);
        file = copy_string_malloc (file);
        remove_end_slash (file); 
        retval = mkdir (file);  
        free_mem_malloc (file);
        break;
        
      case SYS_ISDIR:
        fd = (int)stack_pop (&stack); 
        retval = is_dir (fd);  
       
        break;
      
      case SYS_INUMBER:
        fd = (int)stack_pop (&stack); 
        retval = get_inumber (fd);  
        break;
      
      case SYS_READDIR:
        fd = (int)stack_pop (&stack);
        buffer = (void *)stack_pop (&stack);
        is_addr_valid_n (buffer, READDIR_MAX_LEN + 1);
        buffer2 = allocate_mem_malloc (READDIR_MAX_LEN + 1); 
        retval = readdir (fd, buffer2);  
        memcpy (buffer, buffer2, READDIR_MAX_LEN + 1);
        free_mem_malloc (buffer2);
        break;
        
      default:
        break;
    }
  f->eax = retval;
}

int
readdir (int fd, char *buffer)
{
  ASSERT (fd > 1);
  //check what to do for stdin and stdout
  //fd doesn't uniquely identify file, because fds may be reassigned. 
  // so find any other way of determinig if the same readdir has continued or not
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return 0;
  struct dir *dir = file_get_dir (f);
  if (dir == NULL)
    return 0;
  bool res = dir_readdir (dir, buffer);
  while (res == true && (strcmp (buffer, ".") == 0 || strcmp (buffer, "..") == 0))
      res = dir_readdir (dir, buffer);
  return res;
}

int 
is_dir (int fd)
{
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return 0;
  return file_is_dir (f);
}

block_sector_t 
get_inumber (int fd)
{
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return 0;
  return file_get_inumber (f);
}

bool
remove (const char *file)
{
  return filesys_remove (file);
}

off_t
read (int fd, void *buf, unsigned size)
{
  int i;
  char *buffer = (char *)buf;
  if (fd == STDIN_FILENO)
    {
      i = 0;
      while (i < (int)size)
        {
          buffer[i] = input_getc ();
          i++;
        }
      return i;
    }
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return -1;
  return file_read (f, buffer, size);
}

bool 
create (const char *file, unsigned initial_size)
{
  return filesys_create (file, initial_size);
}

/* Scan the open files of current thread and return the struct file * of file
   assigned descriptor fd */
struct file * get_file_pointer_from_fd (int fd)
{
  struct list *open_files = &(thread_current ()->open_files);
  struct list_elem *e;
  for (e = list_begin (open_files); e != list_end (open_files);
      e = list_next (e))
    {
      struct file_fd *f = list_entry (e, struct file_fd, elem);
      if (f->fd == fd)
        return f->f;
    }
  return NULL;
}

void
close (int fd)
{
  struct file_fd *f = NULL;
  struct list *open_files = &(thread_current ()->open_files);
  struct list_elem *e;
  
  /* Scan the list of open files of current thread to find struct file_fd * of
     file with descriptor fd */
  for (e = list_begin (open_files); e != list_end (open_files);
      e = list_next (e))
    {
      f = list_entry (e, struct file_fd, elem);
      if (f->fd == fd)
        break;
    }
 
  if (e != list_end (open_files))
    {
      /* The order here is important */
      list_remove (e);
      file_close (f->f);
      free_mem_malloc (f);
    }
  
  return;
}

off_t
write (int fd, void *buffer, unsigned size)
{
  if (fd == STDOUT_FILENO)
  {
    putbuf (buffer, size);
    return size;
  } 
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return -1;
  return file_write (f, buffer, size);
}

int
open (const char *file)
{
  struct list *open_files = &(thread_current ()->open_files);
  struct file *fi = filesys_open (file); 
  if (fi == NULL)
    return -1;
  struct file_fd *open_file = (struct file_fd *)allocate_mem_malloc 
                                (sizeof (struct file_fd));
  open_file -> f = fi; 
  open_file -> fd = 3;
  
  /* Find the smallest free file descriptor that is >= 3 */
  struct list_elem *e;
  for (e = list_begin (open_files); e != list_end (open_files);
      e = list_next (e))
    {
      struct file_fd *f = list_entry (e, struct file_fd, elem);
      if (f->fd != open_file->fd)
        break;
      open_file->fd ++;
    }  
  list_insert (e, &open_file->elem);
  return open_file->fd;
}

void
exit (int exit_code)
{
  struct thread *cur = thread_current ();
   
  enum intr_level old_level = intr_disable (); 
  struct thread *par = get_thread_alive (cur->parent);
   /* If the parent of cur is not dead, then exit status of cur has to
      be saved */
  if (par != NULL)
    {
      /* Allocate a struct dead_thread to store exit status of current process */
      struct dead_thread *d = (struct dead_thread *)malloc (
                                sizeof (struct dead_thread));
      d->tid = cur->tid;
      d->parent = cur->parent;
      d->exit_status = exit_code;
      
      list_push_back (&dead_list, &d->elem);
      
    }    
  intr_set_level (old_level);
  
  printf ("%s: exit(%d)\n", cur->name,exit_code);
    
  thread_exit ();
}

void seek (int fd, unsigned position)
{
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return;
  file_seek (f, position);
}

unsigned tell (int fd)
{
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return -1;
  return file_tell (f);
}

int filesize (int fd)
{
  struct file *f = get_file_pointer_from_fd (fd);
  if (f == NULL)
    return -1;
  return file_length (f);
}

mapid_t mmap (int fd, void *addr)
{
  if ((uint32_t)addr % PGSIZE != 0 || pg_no (addr) == 0)
    return -1;
    
  struct file *f = get_file_pointer_from_fd (fd);
  f = file_reopen (f);
  if (f == NULL)
    return -1;
  unsigned old_pos = file_tell (f);
  file_seek (f, 0);
  
  int file_size = file_length (f);
  if (file_size == 0)
    return -1;
  int count_pages = (file_size + PGSIZE - 1) / PGSIZE;
  int i;
  
  /* Check if the required number of pages starting at *addr are unmapped */
  for (i = 0; i < count_pages; i++)
    {
      if (find_spte (addr + i * PGSIZE) != NULL)
        return -1;
    }
    
  struct list *open_mmap = &thread_current ()->open_mmap;
  struct list_elem *e;
  int new_id = 2;
  for (e = list_begin (open_mmap); e != list_end (open_mmap);
        e = list_end (open_mmap))
    {
      if (list_entry (e, struct fd_mmap, elem)->mapid != new_id)
        break;
      new_id++;
    }
  struct fd_mmap *new_mmap = allocate_mem_malloc (sizeof (struct fd_mmap));
  new_mmap->mapid = new_id;
  new_mmap->f = f;
  new_mmap->addr = addr;
  list_insert (e, &new_mmap->elem);
  
  for (i = 0; i < count_pages; i++)
    {
      int bytes_page = (file_size >= PGSIZE? PGSIZE: file_size);
      file_size -= bytes_page;
      add_spte (addr + PGSIZE * i, PGSRC_FILE, (uint32_t)f, bytes_page, PGSIZE * i, 1);
    }
    
  file_seek (f, old_pos);
  return new_id;
}

void munmap (mapid_t mapid)
{ 
  if (mapid == 0 || mapid == 1)
    return;
  struct list *open_mmap = &thread_current ()->open_mmap;
  struct list_elem *e;
  struct fd_mmap *fdm = NULL;
  for (e = list_begin (open_mmap); e != list_end (open_mmap);
        e = list_next (e))
    {
      struct fd_mmap *t = list_entry (e, struct fd_mmap, elem); 
      if (t->mapid == mapid)
        {
          fdm = t;
          break;
        }
    }
  if (fdm == NULL)
    return;
  
  int file_size = file_length (fdm->f);
  int count_pages = (file_size + PGSIZE - 1) / PGSIZE;
  int i;
  for (i = 0; i < count_pages; i++)
      remove_spte (find_spte (fdm->addr + PGSIZE * i));
      
  file_close (fdm->f);
  list_remove (&fdm->elem);
  free_mem_malloc (fdm);
  return;
}

int chdir (char *path)
{
  struct dir *dir = dir_get_dir (path, false);
  if (dir != NULL)
    {  
      thread_set_pwd (dir);
      dir_close (dir);
      return 1;
    }         
  return 0;
}

int mkdir (char *path)
{
  struct dir *dir = dir_get_dir (path, true);
  if (dir == NULL)
    return 0;
  block_sector_t sector;
  int success = 0;
  int i = 0;
  while (path [i] != '\0')
    i++;
  while (i >= 0 && path [i] != '/')
    i--;
  i++;
  if (path [i] == '\0')
    goto mkdir_end;
  path = path + i;
  struct inode *inode;
  if (dir_lookup (dir, path, &inode))
    {
      inode_close (inode);
      goto mkdir_end;
    }
   
  if (!free_map_allocate (1, &sector))
    {
      goto mkdir_end;
    }
  
  if (dir_create (sector, 0) && dir_add (dir, path, sector))
    {
      success = 1;
      goto mkdir_end;
    }
      
  free_map_release (sector, 1);
  
  mkdir_end:
  dir_close (dir);
  //check default value 0 matters or not? if extensible then doens't
  
  return success;
}
