#include "userprog/process.h"
#include "threads/malloc.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "userprog/syscall.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "vm/page_frame.h"
#include "vm/swap.h"

#define PROC_NAME_LIMIT 20

static thread_func start_process NO_RETURN;
static bool load (const char *cmdline, void (**eip) (void), void **esp);

struct alloc_mem
{
  void *mem;
  struct list_elem elem;
};

/* Allocates memory of given size using malloc() 
   Also adds the allocated memory in malloc_mem list of current thread */
void *allocate_mem_malloc (int size)
{
  void *mem = malloc (sizeof (char) * size);
  struct alloc_mem *s = (struct alloc_mem *)malloc (sizeof (struct alloc_mem));
  s->mem = mem;
  list_push_front (&thread_current ()->malloc_mem, &s->elem);
  return mem;
}

/* Free mem and remove it from malloc_mem list of current thread */
void free_mem_malloc (void *mem)
{
  struct list_elem *e;
  struct thread *cur = thread_current ();
  struct alloc_mem *am = NULL;
  for (e = list_begin (&cur->malloc_mem); e != list_end (&cur->malloc_mem);
        e = list_next (e))
    {
      struct alloc_mem *tam = list_entry (e, struct alloc_mem, elem);      
      if (tam->mem == mem)
        {
          am = tam;
          break;
        }
    }
  if (am != NULL)
    {
      list_remove (&am->elem);
      free (am->mem);
      free (am);
    }
  return;
}

/* Starts a new thread running a user program loaded from
   FILENAME.  The new thread may be scheduled (and may even exit)
   before process_execute() returns.  Returns the new process's
   thread id, or TID_ERROR if the thread cannot be created. */
tid_t
process_execute (const char *file_name) 
{
  char *fn_copy;
  tid_t tid;

  /* Make a copy of FILE_NAME.
     Otherwise there's a race between the caller and load(). */
  fn_copy = palloc_get_page (0);
  if (fn_copy == NULL)
    return TID_ERROR;
  strlcpy (fn_copy, file_name, PGSIZE);
  
  /* Extract process name from file_name. Process name is the first part of 
     file_name when split by ' ' */
  char proc_name[PROC_NAME_LIMIT];
  int i = 0;
  while (*(file_name + i) != ' ' && *(file_name + i) != '\0')
    {
      proc_name [i] = *(file_name + i);
      i++;
    }
  proc_name [i] = '\0';
  
  /* Create a new thread to execute FILE_NAME. */
  tid = thread_create (proc_name, PRI_DEFAULT, start_process, fn_copy);
  if (tid == TID_ERROR)
      palloc_free_page (fn_copy); 
  else
    {
      /* Check if the executable loaded successfully.
         The process blocks on its sema_child until the child process
         performs a sema_up () on it.  */
      struct thread *cur = thread_current ();
      cur->process_waiting_for = tid;
      sema_down (&cur->sema_child);
      struct thread *child_t = NULL;
      struct dead_thread *child_dt = NULL;
      enum intr_level old_level = intr_disable ();
      child_t = get_thread_alive (tid);
      child_dt = get_thread_dead (tid);
      if ((child_t == NULL && child_dt == NULL)
        || (child_t != NULL && child_t->executable == NULL))
        { 
          intr_set_level (old_level);
          return TID_ERROR;
        }
      intr_set_level (old_level);
    }
  return tid;
}

/* A thread function that loads a user process and starts it
   running. */
static void
start_process (void *file_name_)
{
  char *file_name = file_name_;
  struct intr_frame if_;
  bool success;
  struct thread *cur = thread_current ();
  hash_init (&cur->spt, spte_hash_func, spte_less_func, NULL);
  
  /* Initialize interrupt frame and load executable. */
  memset (&if_, 0, sizeof if_);
  if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
  if_.cs = SEL_UCSEG;
  if_.eflags = FLAG_IF | FLAG_MBS;
  success = load (file_name, &if_.eip, &if_.esp);

  /* Get parent of thread */
  struct thread *par = get_thread_alive (cur->parent);
  ASSERT (par != NULL);
  if (par->pwd == NULL)
    cur->pwd = dir_open_root ();
  else
    cur->pwd = dir_reopen (par->pwd);
    
  /* If load failed, quit. */
  palloc_free_page (file_name);
  if (!success) 
      thread_exit ();
          
  /* Unblock parent who is waiting for the child to finish loading */
  par->process_waiting_for = -1;
  sema_up (&par->sema_child);

  /* Start the user process by simulating a return from an
     interrupt, implemented by intr_exit (in
     threads/intr-stubs.S).  Because intr_exit takes all of its
     arguments on the stack in the form of a `struct intr_frame',
     we just point the stack pointer (%esp) to our stack frame
     and jump to it. */
  asm volatile ("movl %0, %%esp; jmp intr_exit" : : "g" (&if_) : "memory");
  NOT_REACHED ();
}

/* Waits for thread TID to die and returns its exit status.  If
   it was terminated by the kernel (i.e. killed due to an
   exception), returns -1.  If TID is invalid or if it was not a
   child of the calling process, or if process_wait() has already
   been successfully called for the given TID, returns -1
   immediately, without waiting. */
int
process_wait (tid_t child_tid) 
{
  struct thread *cur = thread_current ();
  struct list_elem *e;
  int retval = -1;
  enum intr_level old_level = intr_disable ();
  int parent_child_tid = get_parent_tid (child_tid);
  /* If child_tid is not present in either all_list or dead_list
    get_parent_tid () will return -1. 
    When process_wait () is called on a dead process for the first
    time, the dead process is removed from the dead_list. Therefore,
    if process_wait () has already been called on child_tid,
    get_parent_tid(child_tid) will return -1 and the following condition will 
    be TRUE */
  if (parent_child_tid != cur->tid)
    return -1;
    
  /* If child has not yet finished, the current process blocks on its sema_child,
     waiting for the child process to finish */
  if (get_thread_dead (child_tid) == NULL)
    {
      cur->process_waiting_for = child_tid;
      sema_down (&cur->sema_child);     
    }
  
  /* Scan dead_list for child_tid */  
  for (e = list_begin (&dead_list); e != list_end (&dead_list);
      e = list_next (e))
    {
      struct dead_thread *d = list_entry (e, struct dead_thread, elem);
      if (d->tid == child_tid)
        {
          retval = d->exit_status;
          list_remove (e);
          free (d);
          break;
        }
    }
  ASSERT (e != list_end (&dead_list));
  intr_set_level (old_level);
  return retval;
}

/* Free the current process's resources. */
void
process_exit (void)
{
  struct thread *cur = thread_current ();
  uint32_t *pd;

  struct list_elem *e, *e2;
  enum intr_level old_level = intr_disable ();  
  /* Remove data of any dead child process.
     As the current process is exiting, it can't call process_wait () on any
     of its child processes. So to prevent memory leak, the data of child 
     processes (which includes their exit status) is removed */
  for (e = list_begin (&dead_list); e != list_end (&dead_list);)
    {
      e2 = e;
      struct dead_thread *dt = list_entry (e, struct dead_thread, elem);
      e = list_next (e);
      if (dt->parent == cur->tid)
        {
          list_remove (e2);
          free (dt);
        }       
    } 
  intr_set_level (old_level);
  
  /* Close all open files of current process */
  for (e = list_begin (&cur->open_files); e != list_end (&cur->open_files);)
    {
      e2 = e;
      e = list_next (e);
      close (list_entry (e2, struct file_fd, elem)->fd);
    }
  
  /* Unmaps alls mmap'd files */
  for (e = list_begin (&cur->open_mmap); e != list_end (&cur->open_mmap);)
    {
      e2 = e;
      e = list_next (e);
      munmap (list_entry (e2, struct fd_mmap, elem)->mapid);
    }
  
  /* Remove all spte from the spt of thread */  
  remove_all_spte ();
    
  /* Remove all entries belonging to the thread in swap_list */
  thread_clean_swap ();
  
  /* Destroy the spt hash table */
  hash_destroy (&cur->spt, NULL);
  
  /* Close the executable that the current process was running */
  if (cur->executable != NULL)
    file_close (cur->executable);
  
  /* Free all malloc'd memory */
  for (e = list_begin (&cur->malloc_mem); e != list_end (&cur->malloc_mem);)
    {
      e2 = e;
      e = list_next (e);
      free_mem_malloc (list_entry (e2, struct alloc_mem, elem)->mem);
    }
  
  struct thread *par = get_thread_alive (cur->parent);
  /* If parent of current process is waiting for current process, 
     then unblock it */
  if (par != NULL && par->process_waiting_for == cur->tid)
    {
      par->process_waiting_for = -1;
      sema_up (&par->sema_child);
    }
  
  /* Destroy the current process's page directory and switch back
     to the kernel-only page directory. */
  pd = cur->pagedir;
  if (pd != NULL) 
    {
      /* Correct ordering here is crucial.  We must set
         cur->pagedir to NULL before switching page directories,
         so that a timer interrupt can't switch back to the
         process page directory.  We must activate the base page
         directory before destroying the process's page
         directory, or our active page directory will be one
         that's been freed (and cleared). */
      cur->pagedir = NULL;
      pagedir_activate (NULL);
      pagedir_destroy (pd);
    }
}

/* Sets up the CPU for running user code in the current
   thread.
   This function is called on every context switch. */
void
process_activate (void)
{
  struct thread *t = thread_current ();

  /* Activate thread's page tables. */
  pagedir_activate (t->pagedir);

  /* Set thread's kernel stack for use in processing
     interrupts. */
  tss_update ();
}

/* We load ELF binaries.  The following definitions are taken
   from the ELF specification, [ELF1], more-or-less verbatim.  */

/* ELF types.  See [ELF1] 1-2. */
typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

/* For use with ELF types in printf(). */
#define PE32Wx PRIx32   /* Print Elf32_Word in hexadecimal. */
#define PE32Ax PRIx32   /* Print Elf32_Addr in hexadecimal. */
#define PE32Ox PRIx32   /* Print Elf32_Off in hexadecimal. */
#define PE32Hx PRIx16   /* Print Elf32_Half in hexadecimal. */

/* Executable header.  See [ELF1] 1-4 to 1-8.
   This appears at the very beginning of an ELF binary. */
struct Elf32_Ehdr
  {
    unsigned char e_ident[16];
    Elf32_Half    e_type;
    Elf32_Half    e_machine;
    Elf32_Word    e_version;
    Elf32_Addr    e_entry;
    Elf32_Off     e_phoff;
    Elf32_Off     e_shoff;
    Elf32_Word    e_flags;
    Elf32_Half    e_ehsize;
    Elf32_Half    e_phentsize;
    Elf32_Half    e_phnum;
    Elf32_Half    e_shentsize;
    Elf32_Half    e_shnum;
    Elf32_Half    e_shstrndx;
  };

/* Program header.  See [ELF1] 2-2 to 2-4.
   There are e_phnum of these, starting at file offset e_phoff
   (see [ELF1] 1-6). */
struct Elf32_Phdr
  {
    Elf32_Word p_type;
    Elf32_Off  p_offset;
    Elf32_Addr p_vaddr;
    Elf32_Addr p_paddr;
    Elf32_Word p_filesz;
    Elf32_Word p_memsz;
    Elf32_Word p_flags;
    Elf32_Word p_align;
  };

/* Values for p_type.  See [ELF1] 2-3. */
#define PT_NULL    0            /* Ignore. */
#define PT_LOAD    1            /* Loadable segment. */
#define PT_DYNAMIC 2            /* Dynamic linking info. */
#define PT_INTERP  3            /* Name of dynamic loader. */
#define PT_NOTE    4            /* Auxiliary info. */
#define PT_SHLIB   5            /* Reserved. */
#define PT_PHDR    6            /* Program header table. */
#define PT_STACK   0x6474e551   /* Stack segment. */

/* Flags for p_flags.  See [ELF3] 2-3 and 2-4. */
#define PF_X 1          /* Executable. */
#define PF_W 2          /* Writable. */
#define PF_R 4          /* Readable. */

static bool setup_stack (void **esp);
static bool validate_segment (const struct Elf32_Phdr *, struct file *);
static bool load_segment (struct file *file, off_t ofs, uint8_t *upage,
                          uint32_t read_bytes, uint32_t zero_bytes,
                          bool writable);

/* Push in stack 'size' bytes starting from address in *data */
static void stack_push (void **stack, void *data, int size)
{
  if (size == 0)
    return;
  if (!(*stack - 1 < PHYS_BASE))
    exit (-1);
  *stack -= size;
 // if (*stack < PHYS_BASE - PGSIZE) 
  /* TRUE if stack page has overflowed */
  //  exit (-1);
 
  memcpy (*stack, data, size);
}

/* Loads an ELF executable from FILE_NAME into the current thread.
   Stores the executable's entry point into *EIP
   and its initial stack pointer into *ESP.
   Returns true if successful, false otherwise. */
bool
load (const char *file_name, void (**eip) (void), void **esp) 
{
  struct thread *cur = thread_current ();
  struct Elf32_Ehdr ehdr;
  struct file *file = NULL;
  off_t file_ofs;
  bool success = false;
  int i;
  
  /* Extract process name from file_name. Process name is the first part of 
     file_name when split by ' ' */
  char proc_name[PROC_NAME_LIMIT];
  i = 0;
  while (*(file_name + i) != ' ' && *(file_name + i) != '\0')
    {
      proc_name [i] = *(file_name + i);
      i++;
    }
  proc_name [i] = '\0';
  
  /* Allocate and activate page directory. */
  cur->pagedir = pagedir_create ();
  if (cur->pagedir == NULL) 
    goto done;
  process_activate ();

  /* Open executable file. */
  file = filesys_open (proc_name);
  if (file == NULL) 
    {
      printf ("load: %s: open failed\n", proc_name);
      goto done; 
    }

  /* Read and verify executable header. */
  if (file_read (file, &ehdr, sizeof ehdr) != sizeof ehdr
      || memcmp (ehdr.e_ident, "\177ELF\1\1\1", 7)
      || ehdr.e_type != 2
      || ehdr.e_machine != 3
      || ehdr.e_version != 1
      || ehdr.e_phentsize != sizeof (struct Elf32_Phdr)
      || ehdr.e_phnum > 1024) 
    {
      printf ("load: %s: error loading executable\n", proc_name);
      goto done; 
    }

  /* Read program headers. */
  file_ofs = ehdr.e_phoff;
  for (i = 0; i < ehdr.e_phnum; i++) 
    {
      struct Elf32_Phdr phdr;

      if (file_ofs < 0 || file_ofs > file_length (file))
        goto done;
      file_seek (file, file_ofs);

      if (file_read (file, &phdr, sizeof phdr) != sizeof phdr)
        goto done;
      file_ofs += sizeof phdr;
      switch (phdr.p_type) 
        {
        case PT_NULL:
        case PT_NOTE:
        case PT_PHDR:
        case PT_STACK:
        default:
          /* Ignore this segment. */
          break;
        case PT_DYNAMIC:
        case PT_INTERP:
        case PT_SHLIB:
          goto done;
        case PT_LOAD:
          if (validate_segment (&phdr, file)) 
            {
              bool writable = (phdr.p_flags & PF_W) != 0;
              uint32_t file_page = phdr.p_offset & ~PGMASK;
              uint32_t mem_page = phdr.p_vaddr & ~PGMASK;
              uint32_t page_offset = phdr.p_vaddr & PGMASK;
              uint32_t read_bytes, zero_bytes;
              if (phdr.p_filesz > 0)
                {
                  /* Normal segment.
                     Read initial part from disk and zero the rest. */
                  read_bytes = page_offset + phdr.p_filesz;
                  zero_bytes = (ROUND_UP (page_offset + phdr.p_memsz, PGSIZE)
                                - read_bytes);
                }
              else 
                {
                  /* Entirely zero.
                     Don't read anything from disk. */
                  read_bytes = 0;
                  zero_bytes = ROUND_UP (page_offset + phdr.p_memsz, PGSIZE);
                }
              if (!load_segment (file, file_page, (void *) mem_page,
                                 read_bytes, zero_bytes, writable))
                goto done;
            }
          else
            goto done;
          break;
        }
    }

  /* Set up stack. */
  if (!setup_stack (esp))
    goto done;
      
  char *t_file_name = allocate_mem_malloc (strlen (file_name) + 1);
  memcpy (t_file_name, file_name, strlen (file_name) + 1);
  char *save_ptr = NULL;
  char *token;
  char *t1_esp = (char *)*esp;
  void *t2_esp;
  int arg_count = 0;
  /* Split file_name by ' ' and push each part into the stack */
  for (token = strtok_r (t_file_name, " ", &save_ptr); token != NULL;
        token = strtok_r (NULL, " ", &save_ptr))
    {
      int len_token = strlen (token);
      stack_push ((void **)&t1_esp, token, len_token + 1);
      arg_count ++;
    }
  free_mem_malloc (t_file_name);
  t2_esp = (void *)t1_esp;
  
  /* Rounding to nearest multiple of 4 to word-align the data in stack */
  while ((uint32_t)t2_esp % 4 != 0)
    {
      uint8_t ui = 0;
      stack_push (&t2_esp, &ui, sizeof (uint8_t));
    }
  char *ch = NULL;
  
  /* Push NULL in stack */
  stack_push (&t2_esp, &ch, sizeof (char *));
  ASSERT (arg_count >= 1);
  
  /* Push in stack the address of each of previously pushed strings */
  stack_push (&t2_esp, &t1_esp, sizeof (char *));
  t1_esp ++;
  while ((void *)t1_esp < *esp)
    {
      if (*(char *)(t1_esp - 1) == '\0')
        stack_push (&t2_esp, &t1_esp, sizeof (char *));
      t1_esp++;
    }
  t1_esp = t2_esp;
  
  /* Push in stack the address where address of first argument is present */
  stack_push (&t2_esp, &t1_esp, sizeof (char *));
  
  /* Push in stack the argument count */
  stack_push (&t2_esp, &arg_count, sizeof (int));
  
  /* Push in stack a fake return address */
  void (*temp)(void); 
  temp = NULL;
  stack_push (&t2_esp, &temp, sizeof (void (*)(void)));
  
  *esp = t2_esp;
  /* Start address. */
  *eip = (void (*) (void)) ehdr.e_entry;
  success = true;

 done:
 /* We arrive here whether the load is successful or not. */
  if (!success)
    {
      file_close (file);
      cur->executable = NULL;
    }
  else
    {
      /* Deny write to the executable of current process */
      file_deny_write (file);
      cur->executable = file;
    }
  return success;
}

/* load() helpers. */


/* Checks whether PHDR describes a valid, loadable segment in
   FILE and returns true if so, false otherwise. */
static bool
validate_segment (const struct Elf32_Phdr *phdr, struct file *file) 
{
  /* p_offset and p_vaddr must have the same page offset. */
  if ((phdr->p_offset & PGMASK) != (phdr->p_vaddr & PGMASK)) 
    return false; 

  /* p_offset must point within FILE. */
  if (phdr->p_offset > (Elf32_Off) file_length (file)) 
    return false;

  /* p_memsz must be at least as big as p_filesz. */
  if (phdr->p_memsz < phdr->p_filesz) 
    return false; 

  /* The segment must not be empty. */
  if (phdr->p_memsz == 0)
    return false;
  
  /* The virtual memory region must both start and end within the
     user address space range. */
  if (!is_user_vaddr ((void *) phdr->p_vaddr))
    return false;
  if (!is_user_vaddr ((void *) (phdr->p_vaddr + phdr->p_memsz)))
    return false;

  /* The region cannot "wrap around" across the kernel virtual
     address space. */
  if (phdr->p_vaddr + phdr->p_memsz < phdr->p_vaddr)
    return false;

  /* Disallow mapping page 0.
     Not only is it a bad idea to map page 0, but if we allowed
     it then user code that passed a null pointer to system calls
     could quite likely panic the kernel by way of null pointer
     assertions in memcpy(), etc. */
  if (phdr->p_vaddr < PGSIZE)
    return false;

  /* It's okay. */
  return true;
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE.  In total, READ_BYTES + ZERO_BYTES bytes of virtual
   memory are initialized, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE
          starting at offset OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   The pages initialized by this function must be writable by the
   user process if WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error
   or disk read error occurs. */
static bool
load_segment (struct file *file, off_t ofs, uint8_t *upage,
              uint32_t read_bytes, uint32_t zero_bytes, bool writable) 
{
  ASSERT ((read_bytes + zero_bytes) % PGSIZE == 0);
  ASSERT (pg_ofs (upage) == 0);
  ASSERT (ofs % PGSIZE == 0);

  file_seek (file, ofs);
  int running_ofs = ofs;
  while (read_bytes > 0 || zero_bytes > 0) 
    {
      /* Calculate how to fill this page.
         We will read PAGE_READ_BYTES bytes from FILE
         and zero the final PAGE_ZERO_BYTES bytes. */
      size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
      size_t page_zero_bytes = PGSIZE - page_read_bytes;
    
      /* Add Supplemental Page Table Entry */
      if (page_read_bytes != 0)
        {
          add_spte (upage, PGSRC_EXECUTABLE, (uint32_t)file, page_read_bytes, 
                  running_ofs, writable);
        //  printf("upage %x type FILE writable %d\n",upage,writable);
        }
      else
        {
          add_spte (upage, PGSRC_ZERO, 0, 0, page_zero_bytes, writable);
        //  printf("upage %x type ZERO writable %d\n",upage,writable);
        }
      
      running_ofs += page_read_bytes;
  
      /* Advance. */
      read_bytes -= page_read_bytes;
      zero_bytes -= page_zero_bytes;
      upage += PGSIZE;
    }
  return true;
}

/* Create a minimal stack by mapping a zeroed page at the top of
   user virtual memory. */
static bool
setup_stack (void **esp) 
{
  bool success = false;

  struct spte *spte = add_spte (((uint8_t *) PHYS_BASE) - PGSIZE, 
                                  PGSRC_STACKZERO, 0, 0, 1, 1); 
  success = put_in_frame (spte);
  if (success) 
    *esp = PHYS_BASE;
 
  return success;
}

