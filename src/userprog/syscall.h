#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include "lib/kernel/list.h"
void syscall_init (void);

void is_addr_valid (void *);
void is_addr_valid_n (void *, int);
int is_string_addr_valid (char *s);
struct file * get_file_pointer_from_fd (int);

void exit (int); 
void close (int);

void munmap (int);

/* struct to associate an open file's struct file * with a 
   file descriptor. A list of struct file_fd is present in every thread's open_files list */
struct file_fd
{
  struct file *f;  
  int fd;
  struct list_elem elem;
};

typedef int mapid_t;

struct fd_mmap
{
  mapid_t mapid;         /* A positive integer that represents mmap id */
  struct file *f;        /* File pointer to the file that is mmap'd */
  void *addr;            /* The user address where the file is mmap'd */
  struct list_elem elem; /* List elem for threads list of mmap'd files */
};
#endif /* userprog/syscall.h */
