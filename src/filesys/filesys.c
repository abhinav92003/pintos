#include "filesys/filesys.h"
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "filesys/file.h"
#include "filesys/free-map.h"
#include "filesys/inode.h"
#include "filesys/directory.h"
#include "userprog/process.h"

/* Partition that contains the file system. */
struct block *fs_device;

static void do_format (void);
int find_filename_begin_index (const char *);

int find_filename_begin_index (const char *path)
{
  int i = 0;
  while (path [i] != '\0')
    i++;
  i--;
  while (i >= 0 && path [i] != '/')
    i--;
  i++;
  return i;
}
/* Initializes the file system module.
   If FORMAT is true, reformats the file system. */
void
filesys_init (bool format) 
{
  fs_device = block_get_role (BLOCK_FILESYS);
  if (fs_device == NULL)
    PANIC ("No file system device found, can't initialize file system.");

  inode_init ();
  free_map_init ();

  if (format) 
    do_format ();
  
  free_map_open ();
}

/* Shuts down the file system module, writing any unwritten data
   to disk. */
void
filesys_done (void) 
{
  remove_all_buffer_cache ();
  free_map_close ();
}


/* Creates a file named NAME with the given INITIAL_SIZE.
   Returns true if successful, false otherwise.
   Fails if a file named NAME already exists,
   or if internal memory allocation fails. */
bool
filesys_create (const char *name, off_t initial_size) 
{
  block_sector_t inode_sector = 0;
  struct dir *dir = dir_get_dir (name, true);
  int fn_begin = find_filename_begin_index (name);
  if (name [fn_begin] == '\0')
    {
      dir_close (dir);
      return 0;
    }
  name = name + fn_begin;
  bool success = (dir != NULL
                  && free_map_allocate (1, &inode_sector)
                  && inode_create (inode_sector, initial_size, false)
                  && dir_add (dir, name, inode_sector));
  if (!success && inode_sector != 0) 
    free_map_release (inode_sector, 1);
  dir_close (dir);

  return success;
}


/* Opens the file with the given NAME.
   Returns the new file if successful or a null pointer
   otherwise.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
struct file *
filesys_open (const char *name)
{
  int i = 0;
  while (name [i] != '\0')
    {
      if (name [i] != '/')
        break;
      i++;
    }
  if (name [i] == '\0' && i > 0) /* Opening the root directory */
    {
      return file_open (inode_open (ROOT_DIR_SECTOR));  
    }
   
  struct dir *dir = dir_get_dir (name, true);
  if (dir == NULL)
    return NULL;
  int fn_begin = find_filename_begin_index (name);
  if (name [fn_begin] == '\0')
    {
      dir_close (dir);
      return NULL;
    }
  name = name + fn_begin;
  struct inode *inode = NULL;

  if (dir != NULL)
    dir_lookup (dir, name, &inode);
  dir_close (dir);
  if (inode == NULL)
    return NULL;
  return file_open (inode);
}

/* Deletes the file named NAME.
   Returns true if successful, false on failure.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
bool
filesys_remove (const char *name) 
{
  int fn_begin = find_filename_begin_index (name);
  struct dir *dir = dir_get_dir (name, true);
  if (name [fn_begin] == '\0')
    {
      dir_close (dir);
      return false;
    }
  name = name + fn_begin;
  if (strcmp (name, ".") == 0 || strcmp (name, "..") == 0)
    return false;
  bool success = dir != NULL && dir_remove (dir, name);
  dir_close (dir); 

  return success;
}

/* Formats the file system. */
static void
do_format (void)
{
  printf ("Formatting file system...");
  free_map_create ();
  if (!dir_create (ROOT_DIR_SECTOR, 16))
    PANIC ("root directory creation failed");
  free_map_close ();
  struct dir *rootdir = dir_open_root ();
  dir_add (rootdir, ".", ROOT_DIR_SECTOR);
  dir_add (rootdir, "..", ROOT_DIR_SECTOR);
  dir_close (rootdir);
  printf ("done.\n");
}
