#include <list.h>
#include "filesys/filesys.h"
#include "filesys/buffer_cache.h"
#include "threads/malloc.h"
#include <string.h>
#include "threads/thread.h"
#include "devices/timer.h"
#include "threads/synch.h"

struct buffer_cache
  {
    uint8_t *data;          /* The block data */
    block_sector_t sector;  /* sector number that the data belongs to */
    bool accessed;          /* Was the buffer cache block recently accessed? */
    bool dirty;             /* Is the buffer cache block dirty */
    struct list_elem elem;  /* list_elem for buffer_cache_list */
  };
struct list buffer_cache_list;
#define BUFFER_CACHE_LIMIT 64
int buffer_cache_list_size;
struct semaphore sema_buffer_cache_access;
struct semaphore sema_buffer_cache_serviceQ;
struct semaphore sema_buffer_cache_reader_count_access;
int buffer_cache_reader_count;

void buffer_cache_write (block_sector_t, const void *);   
void _buffer_cache_write (block_sector_t, const void*, bool, bool);
void buffer_cache_read (block_sector_t, void *);                       
void remove_buffer_cache (block_sector_t sector);
struct buffer_cache* find_buffer_cache (block_sector_t sector);
struct buffer_cache* insert_buffer_cache (const uint8_t *data, 
                                            block_sector_t sector);
void buffer_cache_write_enter (void);
void buffer_cache_write_exit (void);
void buffer_cache_read_enter (void);
void buffer_cache_read_exit (void);                       


static void buffer_cache_periodic_writer (void *);
static void buffer_cache_periodic_reader (void *);
#define BUFFER_CACHE_WRITE_FREQ 5
#define BUFFER_CACHE_READ_FREQ 1

struct list read_ahead_list;
struct lock read_ahead_list_lock;
int read_ahead_list_size;
struct read_ahead
{
  block_sector_t sector;
  struct list_elem elem;
};

void buffer_cache_periodic_reader (void *arg UNUSED)
{
  void *buffer = malloc (BLOCK_SECTOR_SIZE);
  while (1)
    {
      lock_acquire (&read_ahead_list_lock);
      if (list_begin (&read_ahead_list) != list_end (&read_ahead_list))
        {
          while (list_begin (&read_ahead_list) != list_end (&read_ahead_list))
            {
              struct read_ahead *rh = list_entry (list_pop_front (&read_ahead_list), struct read_ahead, elem);
              read_ahead_list_size--;    
              block_sector_t sector = rh->sector;
              free (rh);
              _buffer_cache_write (sector, buffer, true, false); 
              /* 4th argument means don't overwrite in case already there */
            }
        }
      lock_release (&read_ahead_list_lock);
      timer_sleep (TIMER_FREQ * BUFFER_CACHE_READ_FREQ);
    }
  free (buffer);
}

void buffer_cache_write_enter ()
{
  sema_down (&sema_buffer_cache_serviceQ);
  sema_down (&sema_buffer_cache_access);
  sema_up (&sema_buffer_cache_serviceQ);
}

void buffer_cache_write_exit ()
{
  sema_up (&sema_buffer_cache_access);
}

void buffer_cache_read_enter ()
{
  sema_down (&sema_buffer_cache_serviceQ);
  sema_down (&sema_buffer_cache_reader_count_access);
  if (buffer_cache_reader_count == 0)
    sema_down (&sema_buffer_cache_access);
  buffer_cache_reader_count++;      
  sema_up (&sema_buffer_cache_reader_count_access);
  sema_up (&sema_buffer_cache_serviceQ);
}

void buffer_cache_read_exit ()
{
  sema_down (&sema_buffer_cache_reader_count_access);
  buffer_cache_reader_count--;
  if (buffer_cache_reader_count == 0)
    sema_up (&sema_buffer_cache_access);
  sema_up (&sema_buffer_cache_reader_count_access);
}


/* Function for thread "buffer_cache_write". It periodically writes all
   dirty buffer cache sectors to file system block device */
static void
buffer_cache_periodic_writer (void *arg UNUSED)
{
  while (1)
  {
    buffer_cache_write_all ();
    timer_sleep (TIMER_FREQ * BUFFER_CACHE_WRITE_FREQ); 
  }
}

struct buffer_cache*
find_buffer_cache (block_sector_t sector)
{
  struct list_elem *e;
  for (e = list_begin (&buffer_cache_list); e != list_end (&buffer_cache_list);
        e = list_next (e))
    {
      struct buffer_cache *t = list_entry (e, struct buffer_cache, elem);
      if (t->sector == sector)
        return t;
    }
  return NULL;
}

struct buffer_cache*
insert_buffer_cache (const uint8_t *data, block_sector_t sector)
{
  if (buffer_cache_list_size == BUFFER_CACHE_LIMIT)
    {
      while (true)
        {
          struct list_elem *e = list_begin (&buffer_cache_list);
          struct buffer_cache *bc = list_entry (e, struct buffer_cache, elem);
          if (bc->accessed)
            {
              bc->accessed = false;
              list_remove (e);
              list_push_back (&buffer_cache_list, e);  
            }
          else
            {
              remove_buffer_cache (bc->sector);
              break;
            }
        } 
    }
    
  struct buffer_cache *new_bc = malloc (sizeof (struct buffer_cache));
  new_bc->data = malloc (BLOCK_SECTOR_SIZE);
  /* new_bc->data == NULL, new_bc == NULL condition check */
  memcpy (new_bc->data, data, BLOCK_SECTOR_SIZE);
  new_bc->sector = sector;
  new_bc->accessed = false;
  new_bc->dirty = false;
   
  buffer_cache_list_size ++;
  list_push_back (&buffer_cache_list, &new_bc->elem);
  return new_bc;
}

void 
remove_buffer_cache (block_sector_t sector)
{
  struct list_elem *e;
  for (e = list_begin (&buffer_cache_list); e != list_end (&buffer_cache_list);
        e = list_next (e))
    {
      struct buffer_cache *t = list_entry (e, struct buffer_cache, elem);
      if (t->sector == sector)
        {
          if (t->dirty)
            block_write (fs_device, sector, t->data);
          free (t->data);
          list_remove (e);
          buffer_cache_list_size --;
          free (t);
          return; 
        }
    }
  return;

}

void
buffer_cache_write_all ()
{
  buffer_cache_write_enter ();
  struct list_elem *e;
  for (e = list_begin (&buffer_cache_list); e != list_end (&buffer_cache_list);
        e = list_next (e))
    {
      struct buffer_cache *t = list_entry (e, struct buffer_cache, elem);
      if (t->dirty)
        {
          block_write (fs_device, t->sector, t->data);
          t->dirty = false;
        }
    }
  buffer_cache_write_exit ();
}

void 
remove_all_buffer_cache ()
{
  buffer_cache_write_enter ();
  struct list_elem *e, *e2;
  for (e = list_begin (&buffer_cache_list); e != list_end (&buffer_cache_list);)
    {
      e2 = e;
      e = list_next (e);
      remove_buffer_cache (list_entry (e2, struct buffer_cache, elem)->sector);
    }
  buffer_cache_write_exit ();
}


void buffer_cache_read (block_sector_t sector, void* buffer)
{
  buffer_cache_read_enter ();
  struct buffer_cache *bc = find_buffer_cache (sector);
  if (bc == NULL)
    {
      buffer_cache_read_exit ();
      _buffer_cache_write (sector, buffer, true, false); 
     
    }
  else 
    {
      memcpy (buffer, bc->data, BLOCK_SECTOR_SIZE);
      bc->accessed = true;
      buffer_cache_read_exit ();
    }
  
  /* Read-ahead for next sector */  
  lock_acquire (&read_ahead_list_lock);
  if (sector + 1 < block_size (fs_device))
    {
      struct read_ahead *rh = malloc (sizeof (struct read_ahead));
      rh->sector = sector + 1;      
      read_ahead_list_size++;
      list_push_back (&read_ahead_list, &rh->elem);
      if (read_ahead_list_size > BUFFER_CACHE_LIMIT / 2)      
      /* This is to prevent overflooding buffer cache with read-ahead requests
         and to keep only the latest entries */
        {
          rh = list_entry (list_pop_front (&read_ahead_list), struct read_ahead, elem);
          read_ahead_list_size--;
          free (rh);
        }
      
      
    }
  lock_release (&read_ahead_list_lock);
}

void buffer_cache_write (block_sector_t sector, const void* buffer)
{
  _buffer_cache_write (sector, buffer, false, true);
}

/* if read == true, sector has to be read from fs_device and added 
   to buffer cache.
   if read == false, the provided buffer has to be written to 
   buffer cache. 
   This functionalty is required when block_read is called on a 
   block that is not present in buffer cache. This functionality
   helps to read and write the new block to buffer cache while
   not releasing the semaphore.  
   The buffer_cache_write () that is called from other files, calls
   this function with read == false.  */

void
_buffer_cache_write (block_sector_t sector, const void* buffer, bool read, bool overwrite)
{
  buffer_cache_write_enter ();
  if (read) /* If read == true, this is important. Even though buffer is const */
    block_read (fs_device, sector, buffer);
 
  struct buffer_cache *bc = find_buffer_cache (sector);   
  if (bc == NULL)
    {
      bc = insert_buffer_cache (buffer, sector);
      bc->accessed = true;
      bc->dirty = true;
    }
  else if (overwrite)
    {
      memcpy (bc->data, buffer, BLOCK_SECTOR_SIZE);
      bc->accessed = true;
      bc->dirty = true;
    }
  
  buffer_cache_write_exit ();
}

void buffer_cache_init ()
{
  list_init (&buffer_cache_list);
  buffer_cache_list_size = 0;
  sema_init (&sema_buffer_cache_access, 1);
  sema_init (&sema_buffer_cache_serviceQ, 1);
  sema_init (&sema_buffer_cache_reader_count_access, 1);
  buffer_cache_reader_count = 0;
  list_init (&read_ahead_list);
  lock_init (&read_ahead_list_lock);
  read_ahead_list_size = 0;
  
  thread_create ("bc_writer", PRI_DEFAULT, buffer_cache_periodic_writer, NULL);
  thread_create ("bc_reader", PRI_DEFAULT, buffer_cache_periodic_reader, NULL);
}

