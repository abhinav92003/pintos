#ifndef FILESYS_BUFFER_CACHE_H
#define FILESYS_BUFFER_CACHE_H
#include "devices/block.h"

void buffer_cache_init (void);
void buffer_cache_read (block_sector_t, void*);
void buffer_cache_write (block_sector_t, const void*);
void remove_all_buffer_cache (void);
void buffer_cache_write_all (void);

#endif /* filesys/buffer_cache.h */
