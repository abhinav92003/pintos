#include "filesys/directory.h"
#include <stdio.h>
#include <string.h>
#include <list.h>
#include "filesys/filesys.h"
#include "filesys/inode.h"
#include "threads/malloc.h"
#include "threads/thread.h"

/* A directory. */
struct dir 
  {
    struct inode *inode;                /* Backing store. */
    struct lock dir_lock;               /* Lock to synchronize accesses to dir */
    off_t pos;                          /* Current position. */
  };

/* A single directory entry. */
struct dir_entry 
  {
    block_sector_t inode_sector;        /* Sector number of header. */
    char name[NAME_MAX + 1];            /* Null terminated file name. */
    bool in_use;                        /* In use or free? */
  };

/* Creates a directory with space for ENTRY_CNT entries in the
   given SECTOR.  Returns true if successful, false on failure. */
bool
dir_create (block_sector_t sector, size_t entry_cnt)
{
  return inode_create (sector, (entry_cnt + 2) * sizeof (struct dir_entry), true);
}

/* Opens and returns the directory for the given INODE, of which
   it takes ownership.  Returns a null pointer on failure. */
struct dir *
dir_open (struct inode *inode) 
{
  if (inode_is_removed (inode))
    return false;
  struct dir *dir = calloc (1, sizeof *dir);
  if (inode != NULL && dir != NULL)
    {
      dir->inode = inode;
      dir->pos = 0;
      return dir;
    }
  else
    {
      inode_close (inode);
      free (dir);
      return NULL; 
    }
}

/* Opens the root directory and returns a directory for it.
   Return true if successful, false on failure. */
struct dir *
dir_open_root (void)
{
  return dir_open (inode_open (ROOT_DIR_SECTOR));
}

/* Opens and returns a new directory for the same inode as DIR.
   Returns a null pointer on failure. */
struct dir *
dir_reopen (struct dir *dir) 
{
  if (dir == NULL)
    return NULL;
  if (inode_is_removed (dir->inode))
    return NULL;
  return dir_open (inode_reopen (dir->inode));
}

/* Destroys DIR and frees associated resources. */
void
dir_close (struct dir *dir) 
{
  if (dir != NULL)
    {
      inode_close (dir->inode);
      free (dir);
    }
}

/* Returns the inode encapsulated by DIR. */
struct inode *
dir_get_inode (struct dir *dir) 
{
  return dir->inode;
}

/* Searches DIR for a file with the given NAME.
   If successful, returns true, sets *EP to the directory entry
   if EP is non-null, and sets *OFSP to the byte offset of the
   directory entry if OFSP is non-null.
   otherwise, returns false and ignores EP and OFSP. */
static bool
lookup (const struct dir *dir, const char *name,
        struct dir_entry *ep, off_t *ofsp) 
{
  /* Make sure that dir->inode->dir_lock has been acquired  */
  
  struct dir_entry e;
  size_t ofs;
  
  ASSERT (dir != NULL);
  ASSERT (name != NULL);
  
  for (ofs = 0; inode_read_at (dir->inode, &e, sizeof e, ofs) == sizeof e;
       ofs += sizeof e) 
    if (e.in_use && !strcmp (name, e.name)) 
      {
        if (ep != NULL)
          *ep = e;
        if (ofsp != NULL)
          *ofsp = ofs;
        return true;
      }
  return false;
}

/* Searches DIR for a file with the given NAME
   and returns true if one exists, false otherwise.
   On success, sets *INODE to an inode for the file, otherwise to
   a null pointer.  The caller must close *INODE. */
bool
dir_lookup (const struct dir *dir, const char *name,
            struct inode **inode) 
{
  struct dir_entry e;

  ASSERT (dir != NULL);
  ASSERT (name != NULL);

  inode_lock_dir (dir->inode);
  if (lookup (dir, name, &e, NULL))
    *inode = inode_open (e.inode_sector);
  else
    *inode = NULL;
  inode_unlock_dir (dir->inode);

  return *inode != NULL;
}

/* Adds a file named NAME to DIR, which must not already contain a
   file by that name.  The file's inode is in sector
   INODE_SECTOR.
   Returns true if successful, false on failure.
   Fails if NAME is invalid (i.e. too long) or a disk or memory
   error occurs. */
bool
dir_add (struct dir *dir, const char *name, block_sector_t inode_sector)
{
  struct dir_entry e;
  off_t ofs;
  bool success = false;

  ASSERT (dir != NULL);
  ASSERT (name != NULL);

  /* Check NAME for validity. */
  if (*name == '\0' || strlen (name) > NAME_MAX)
    return false;

  inode_lock_dir (dir->inode);
  
  /* Check that NAME is not in use. */
  if (lookup (dir, name, NULL, NULL))
    goto add_done;

  /* Set OFS to offset of free slot.
     If there are no free slots, then it will be set to the
     current end-of-file.
     
     inode_read_at() will only return a short read at end of file.
     Otherwise, we'd need to verify that we didn't get a short
     read due to something intermittent such as low memory. */
  for (ofs = 0; inode_read_at (dir->inode, &e, sizeof e, ofs) == sizeof e;
       ofs += sizeof e) 
    if (!e.in_use)
      break;

  /* Write slot. */
  e.in_use = true;
  strlcpy (e.name, name, sizeof e.name);
  e.inode_sector = inode_sector;
  
  if (strcmp (name, ".") != 0 && strcmp (name, "..") != 0)
    {
      struct inode *added_inode = inode_open (inode_sector);
      if (inode_is_dir (added_inode))
        {
          struct dir *added_dir = dir_open (added_inode);
          dir_add (added_dir, ".", inode_sector);
          dir_add (added_dir, "..", inode_get_inumber (dir->inode));
        }
      inode_close (added_inode);
    }
  success = inode_write_at (dir->inode, &e, sizeof e, ofs) == sizeof e;
  
 add_done:
  inode_unlock_dir (dir->inode);
  return success;
}

/*
void print_all_files (struct dir *dir)
{
  printf("files are\n");
  struct dir *d = dir_reopen (dir);
  char str [20];
  while (dir_readdir (d, str))
    {
      printf("%s\n",str);
    }
}*/

/* Caller has to close the struct dir */
struct dir *
dir_get_dir (const char *path, bool is_fn_atend)
{
  int len = strlen (path);
  if (len == 0)
    {
      if (is_fn_atend)
        return NULL;
      return thread_get_pwd ();  
    }
  if (is_fn_atend && len > 0)
    {
      len--;
      while (len >= 0 && path [len] != '/')
        len--;
      if (len < 0)
        len = 0;
    }
  int i = 0;
  struct dir *dir = NULL;
  
  if (path [0] == '/')
      dir = dir_open_root ();
  else
      dir = thread_get_pwd ();
  if (dir == NULL)
    return NULL;
  while (1)
    {
      while (path [i] == '/')
        i++;
      char next [NAME_MAX + 1];
      int j = 0;
      while (i < len && path [i] != '/')
        {
          next [j] = path [i];
          j++;
          i++;
        }
      next [j] = '\0';
      if (j == 0)
          return dir;
      struct inode *inode;
      if (!dir_lookup (dir, next, &inode))
        {
          dir_close (dir);
          return NULL;
        }
      dir_close (dir);
      if (inode_is_dir (inode))
        {
          dir = dir_open (inode_reopen (inode));             
          if (dir == NULL)
            return NULL;  
        }
      else
        {
          inode_close (inode);
          return NULL;
        }
    }
  return dir;
}

/* Removes any entry for NAME in DIR.
   Returns true if successful, false on failure,
   which occurs only if there is no file with the given NAME. */
bool
dir_remove (struct dir *dir, const char *name) 
{
  struct dir_entry e;
  struct inode *inode = NULL;
  bool success = false;
  off_t ofs;
  ASSERT (dir != NULL);
  ASSERT (name != NULL);
  if (strlen (name) == 0)
    return false;
  ASSERT (strcmp (name, ".") != 0 &&  strcmp (name, "..") != 0);
  
  inode_lock_dir (dir->inode);
  /* Find directory entry. */
  if (!lookup (dir, name, &e, &ofs))
      goto remove_done;

  /* Open inode. */
  inode = inode_open (e.inode_sector);
  if (inode == NULL)
    goto remove_done;
  
  if (inode_get_inumber (inode) == ROOT_DIR_SECTOR)
    goto remove_done;
    
  if (inode_is_dir (inode) && (strcmp (name, ".") != 0 &&  strcmp (name, "..") != 0))
    {
      struct dir *d_dir = dir_open (inode);
      char subfile_name [NAME_MAX + 1];
      while (dir_readdir (d_dir, subfile_name))
        {
          if (!(strcmp (subfile_name, ".") == 0 || strcmp (subfile_name, "..") == 0))
            {
              goto remove_done;
            }
        }
    }
  /* Erase directory entry. */
  e.in_use = false;
  if (inode_write_at (dir->inode, &e, sizeof e, ofs) != sizeof e) 
    goto remove_done;

  /* Remove inode. */
  inode_remove (inode);
  success = true;
 remove_done:
  inode_unlock_dir (dir->inode);
  inode_close (inode);
  return success;
}

/* Reads the next directory entry in DIR and stores the name in
   NAME.  Returns true if successful, false if the directory
   contains no more entries. */
bool
dir_readdir (struct dir *dir, char name[NAME_MAX + 1])
{
  struct dir_entry e;

  while (inode_read_at (dir->inode, &e, sizeof e, dir->pos) == sizeof e) 
    {
      dir->pos += sizeof e;
      if (e.in_use)
        {
          strlcpy (name, e.name, NAME_MAX + 1);
          return true;
        } 
    }
  dir->pos = 0;
  return false;
}
