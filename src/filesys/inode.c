#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "devices/timer.h"
#include "filesys/directory.h"

/* Identifies an inode. */
#define INODE_MAGIC 0x494e4f44

#define COUNT_DIRECT_BLOCK_POINTERS 123
#define ADDR_PER_SECTOR (BLOCK_SECTOR_SIZE / sizeof (block_sector_t))

#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) < (b) ? (a) : (b))

/* On-disk inode.
   Must be exactly BLOCK_SECTOR_SIZE bytes long. */
struct inode_disk
  {
    off_t length;                       /* File size in bytes. */
    bool is_dir;                        /* true if inode represents a directory */
    block_sector_t singly_indirect;     /* Sector containing singly indirect pointers */
    block_sector_t doubly_indirect;     /* Sector containing doubly indirect pointers */
    block_sector_t direct [COUNT_DIRECT_BLOCK_POINTERS];         
                                        /* Direct pointers to sectors */
    unsigned magic;                     /* Magic number. */
    
  };




void block_write_wrapper (struct block *block, block_sector_t sector, 
                            const uint8_t *buffer);
void block_read_wrapper (struct block *block, block_sector_t sector, 
                            void *buffer);

void get_all_block_counts (off_t, block_sector_t*);
block_sector_t doubly_get_block (block_sector_t block_id, block_sector_t doubly_sector);
block_sector_t singly_get_block (block_sector_t block_id, block_sector_t singly_sector);

bool allocate_singly_indirect (block_sector_t count_direct_old, block_sector_t count_direct_new, block_sector_t *si_blockp);
bool allocate_doubly_indirect (block_sector_t count_singly_old, block_sector_t count_direct_last_old, block_sector_t count_singly_new, block_sector_t count_direct_last_new, block_sector_t *di_blockp);
void deallocate_singly_indirect (block_sector_t count_direct_old, block_sector_t count_direct_new, block_sector_t si_block);
void deallocate_doubly_indirect (block_sector_t count_singly, block_sector_t count_direct_last, 
                                    block_sector_t di_block);
bool inode_extend (struct inode_disk *disk_inode, block_sector_t inode_sector, off_t new_length);



void
block_read_wrapper (struct block *block, block_sector_t sector, void *buffer)
{
  ASSERT (block == fs_device);
  buffer_cache_read (sector, buffer);
  return;
}

void
block_write_wrapper (struct block *block, block_sector_t sector, const uint8_t *buffer)
{
  ASSERT (block == fs_device);
  buffer_cache_write (sector, buffer);
  return;
}


/* Returns the number of sectors to allocate for an inode SIZE
   bytes long. */
static inline size_t
bytes_to_sectors (off_t size)
{
  return DIV_ROUND_UP (size, BLOCK_SECTOR_SIZE);
}

/* In-memory inode. */
struct inode 
  {
    struct list_elem elem;              /* Element in inode list. */
    block_sector_t sector;              /* Sector number of disk location. */
    int open_cnt;                       /* Number of openers. */
    bool removed;                       /* True if deleted, false otherwise. */
    int deny_write_cnt;                 /* 0: writes ok, >0: deny writes. */
    struct lock metadata_lock;          /* Lock for accessing/modifying metadata */
    struct lock data_reader_count_lock; /* Lock for data_reader_count variable */
    struct semaphore data_lock;         /* Lock for writer and first reader */
    struct lock data_service_Q;         /* Queue to ensure fair access */
    int data_reader_count;              /* Number of readers currently active */
    
    struct lock dir_lock;               /* Lock to synchronize directory 
                                           modifications on a directory inode */
    struct inode_disk data;             /* Inode content. */
  };

void inode_data_read_enter (struct inode *inode);
void inode_data_read_exit (struct inode *inode);
void inode_data_write_enter (struct inode *inode);
void inode_data_write_exit (struct inode *inode);

void inode_data_read_enter (struct inode *inode)
{
  lock_acquire (&inode->data_service_Q);
  lock_acquire (&inode->data_reader_count_lock);
  if (inode->data_reader_count == 0)
    sema_down (&inode->data_lock);
  inode->data_reader_count++;
  lock_release (&inode->data_reader_count_lock);
  lock_release (&inode->data_service_Q);
}

void inode_data_read_exit (struct inode *inode)
{
  lock_acquire (&inode->data_reader_count_lock);
  inode->data_reader_count--;
  if (inode->data_reader_count == 0)
    sema_up (&inode->data_lock);
  lock_release (&inode->data_reader_count_lock);
}

void inode_data_write_enter (struct inode *inode)
{
  lock_acquire (&inode->data_service_Q);
  sema_down (&inode->data_lock);
  lock_release (&inode->data_service_Q);
}

void inode_data_write_exit (struct inode *inode)
{
  sema_up (&inode->data_lock);
}

static char zeros [BLOCK_SECTOR_SIZE];
block_sector_t singly_get_block (block_sector_t block_id, block_sector_t singly_sector)
{
  ASSERT (block_id < ADDR_PER_SECTOR);
  uint32_t *block = calloc (ADDR_PER_SECTOR, sizeof (block_sector_t));
  block_read_wrapper (fs_device, singly_sector, block);
  block_sector_t result = block [block_id];
  free (block);
  return result;
}

block_sector_t doubly_get_block (block_sector_t block_id, block_sector_t doubly_sector)
{
  ASSERT (block_id < ADDR_PER_SECTOR * ADDR_PER_SECTOR);
  uint32_t *block = calloc (ADDR_PER_SECTOR, sizeof (block_sector_t));
  block_read_wrapper (fs_device, doubly_sector, block);
  block_sector_t singly_block_id = block_id / ADDR_PER_SECTOR;
  block_sector_t result = singly_get_block (block_id % ADDR_PER_SECTOR, block [singly_block_id]);
  free (block);
  return result;
}

/* Returns the block device sector that contains byte offset POS
   within INODE.
   Returns -1 if INODE does not contain data for a byte at offset
   POS. */
static block_sector_t
byte_to_sector (struct inode *inode, off_t pos) 
{
  ASSERT (inode != NULL);
  block_sector_t sector = -1;
  lock_acquire (&inode->metadata_lock);
  if (pos < inode->data.length)
    {
      block_sector_t seq_block_id = pos / BLOCK_SECTOR_SIZE;
      if (seq_block_id < COUNT_DIRECT_BLOCK_POINTERS)
        sector = inode->data.direct [seq_block_id];
      else
        {
       
          seq_block_id -= COUNT_DIRECT_BLOCK_POINTERS;
          if (seq_block_id < ADDR_PER_SECTOR)
            {
              sector = singly_get_block (seq_block_id, inode->data.singly_indirect);
            }
          else
            {
              seq_block_id -= ADDR_PER_SECTOR;       
              sector = doubly_get_block (seq_block_id, inode->data.doubly_indirect);
            }
        }
    }
  lock_release (&inode->metadata_lock);
  return sector;
}

/* List of open inodes, so that opening a single inode twice
   returns the same `struct inode'. */
static struct list open_inodes;
struct lock inode_list_lock;

void get_all_block_counts (off_t length, block_sector_t count[4])
{
  /* count[0] = count of occupied direct block pointers
     count[1] = count of occupied direct block pointers 
                in singly indirect block
     count[2] = count of occupied singly indirect block 
                pointers in doubly indirect block
     count[3] = count of occupied direct block pointers 
                in the last partially filled singly 
                indirect block   */
  
  block_sector_t seq_blocks = DIV_ROUND_UP (length, BLOCK_SECTOR_SIZE);
  count[0] = count[1] = count[2] = count[3] = 0;
  count[0] = MIN (COUNT_DIRECT_BLOCK_POINTERS, seq_blocks);
  if (seq_blocks < COUNT_DIRECT_BLOCK_POINTERS)
    seq_blocks = 0;
  else
    seq_blocks = seq_blocks - COUNT_DIRECT_BLOCK_POINTERS;
  if (seq_blocks == 0)
    return;
  count[1] = MIN (ADDR_PER_SECTOR, seq_blocks);
  if (seq_blocks < ADDR_PER_SECTOR)
    seq_blocks = 0;
  else
    seq_blocks = seq_blocks - ADDR_PER_SECTOR;
  if (seq_blocks == 0)
    return;
  count[2] = DIV_ROUND_UP (seq_blocks , ADDR_PER_SECTOR);
  count[3] = seq_blocks % ADDR_PER_SECTOR;
  if (count [3] == 0)
    count [3] = ADDR_PER_SECTOR;
  return;
}

/* Initializes the inode module. */
void
inode_init (void) 
{
  lock_init (&inode_list_lock);
  list_init (&open_inodes);
  buffer_cache_init ();
}

bool allocate_singly_indirect (block_sector_t count_direct_old, block_sector_t count_direct_new, block_sector_t *si_blockp)
{
  ASSERT (count_direct_new > count_direct_old);
  uint32_t *block = calloc (ADDR_PER_SECTOR, sizeof (block_sector_t));
  if (block == NULL)
    return false;
  bool new_singly = false;
  if (count_direct_old == 0) /* Means we are creating a new singly indirect block */
    new_singly = true;
  if (!new_singly) 
    {
      block_read_wrapper (fs_device, *si_blockp, block);
    }
  else /* Means we are allocating a new singly indirect block */
    {
      if (!free_map_allocate (1, si_blockp))
        {
          free (block);
          return false;
        }
    }
  block_sector_t i;;
  
  for (i = count_direct_old; i < count_direct_new; i++)
    {
      if (!free_map_allocate (1, &block [i]))
        break;
      block_write_wrapper (fs_device, block [i],  (uint8_t *) zeros);
    }
  if (i != count_direct_new && i != count_direct_old)
    {
      i--;
      while (1)
        {
          free_map_release (block [i], 1);
          if (i == count_direct_old)
            break;
          i--;
        }
      if (new_singly)
        free_map_release (*si_blockp, 1);
      free (block);
      return false;
    }
  block_write_wrapper (fs_device, *si_blockp, (uint8_t *)block);
  free (block);
  return true;
}

void deallocate_singly_indirect (block_sector_t count_direct_old, block_sector_t count_direct_new, block_sector_t si_block)
{
  ASSERT (count_direct_old != count_direct_new);
  uint32_t *block = calloc (ADDR_PER_SECTOR, sizeof (block_sector_t));
  block_read_wrapper (fs_device, si_block, block);
  block_sector_t i;
  for (i = count_direct_old - 1; i >= count_direct_new; i++)
    {
      free_map_release (block [i], 1);
    }
  if (count_direct_new == 0)
    free_map_release (si_block, 1);
  free (block);
  return;
}

bool allocate_doubly_indirect (block_sector_t count_singly_old, block_sector_t count_direct_last_old,block_sector_t count_singly_new, block_sector_t count_direct_last_new, block_sector_t *di_blockp)
{
  ASSERT (count_singly_new > count_singly_old || count_direct_last_new > count_direct_last_old);
  ASSERT (count_direct_last_old <= ADDR_PER_SECTOR && count_direct_last_new <= ADDR_PER_SECTOR);
  bool new_doubly = false;
  if (count_direct_last_old == 0 && count_singly_old == 0)
    new_doubly = true;
    
  if (count_direct_last_old == 0)
    count_direct_last_old = ADDR_PER_SECTOR;
  if (count_direct_last_new == 0)
    count_direct_last_new = ADDR_PER_SECTOR;
    
  uint32_t *block = calloc (ADDR_PER_SECTOR, sizeof (block_sector_t));
  if (block == NULL)
    return false;
  if (!new_doubly) /* Means we are extending an old doubly indirect block */
    {
      block_read_wrapper (fs_device, *di_blockp, block);
    }
  else  /* Means we are allocating a new doubly indirect block */
    {
      if (!free_map_allocate (1, di_blockp))
        {
          free (block);
          return false;
        }
    }
  block_sector_t i;
  
  if (!new_doubly) /* If this is not a new doubly block, then extend the last incomplete singly block */
    {
      if (count_direct_last_old != ADDR_PER_SECTOR)
        allocate_singly_indirect (count_direct_last_old, (count_singly_old < count_singly_new ? ADDR_PER_SECTOR : count_direct_last_new), &block [count_singly_old - 1]);
    }
  for (i = count_singly_old; i < count_singly_new; i++)
    {
      if (!allocate_singly_indirect (0, (i != (count_singly_new - 1) ? ADDR_PER_SECTOR : count_direct_last_new), &block [i]))
        break;
    }
  if (i != count_singly_new)
    {
      if (i != count_singly_old)
        {
          i--;
          while (1)
            {
              deallocate_singly_indirect (ADDR_PER_SECTOR, 0, block [i]);
              if (i == 0)
                break;
              i--;
            }
        }
      if (!new_doubly)
        {
          if (count_direct_last_old != ADDR_PER_SECTOR)
            deallocate_singly_indirect ((count_singly_old < count_singly_new ? ADDR_PER_SECTOR : count_direct_last_new), count_direct_last_old, block [count_singly_old - 1]);
        }
      if (new_doubly)
        free_map_release (*di_blockp, 1);
      free (block);
      return false;
    }
  block_write_wrapper (fs_device, *di_blockp, (uint8_t *)block);
  free (block);
  return true;
}

void deallocate_doubly_indirect (block_sector_t count_singly, block_sector_t count_direct_last, block_sector_t di_block)
{
  ASSERT (count_singly != 0 && count_direct_last != 0);
  uint32_t *block = calloc (ADDR_PER_SECTOR, sizeof (block_sector_t));
  block_read_wrapper (fs_device, di_block, block);
  block_sector_t i;
  for (i = 0; i < count_singly - 1; i++)
    {
      deallocate_singly_indirect (ADDR_PER_SECTOR, 0, block [i]);
    }
  deallocate_singly_indirect (count_direct_last, 0, block [i]);
  free_map_release (di_block, 1);
  free (block);
  return;
}


/* Returns true if inode is for a directory */
bool
inode_is_dir (struct inode *inode)
{
  return inode->data.is_dir;
}

/* Initializes an inode with LENGTH bytes of data and
   writes the new inode to sector SECTOR on the file system
   device.
   Returns true if successful.
   Returns false if memory or disk allocation fails. */
bool
inode_create (block_sector_t sector, off_t length, bool is_dir)
{
  struct inode_disk *disk_inode = NULL;
  bool success = false;

  ASSERT (length >= 0);

  /* If this assertion fails, the inode structure is not exactly
     one sector in size, and you should fix that. */
  ASSERT (sizeof *disk_inode == BLOCK_SECTOR_SIZE);

  
  disk_inode = calloc (1, sizeof *disk_inode);
  if (disk_inode != NULL)
    {
      disk_inode->length = 0;
      disk_inode->is_dir = is_dir;
      disk_inode->magic = INODE_MAGIC;
      success = inode_extend (disk_inode, sector, length);
      free (disk_inode);
    }
  return success;
}

/* Reads an inode from SECTOR
   and returns a `struct inode' that contains it.
   Returns a null pointer if memory allocation fails. */
struct inode *
inode_open (block_sector_t sector)
{
  struct list_elem *e;
  struct inode *inode;

  lock_acquire (&inode_list_lock);
  /* Check whether this inode is already open. */
  for (e = list_begin (&open_inodes); e != list_end (&open_inodes);
       e = list_next (e)) 
    {
      inode = list_entry (e, struct inode, elem);
      if (inode->sector == sector) 
        {
          inode_reopen (inode);
          lock_release (&inode_list_lock);         
          return inode; 
        }
    }

  /* Allocate memory. */
  inode = malloc (sizeof *inode);
  if (inode == NULL)
    return NULL;

  /* Initialize. */
  inode->sector = sector;
  inode->open_cnt = 1;
  inode->deny_write_cnt = 0;
  inode->removed = false;
  lock_init (&inode->data_reader_count_lock);
  sema_init (&inode->data_lock, 1);
  lock_init (&inode->metadata_lock);
  lock_init (&inode->data_service_Q);
  lock_init (&inode->dir_lock);
  inode->data_reader_count = 0;
  block_read_wrapper (fs_device, inode->sector, &inode->data);
  list_push_front (&open_inodes, &inode->elem);
  lock_release (&inode_list_lock);
  return inode;
}

/* Reopens and returns INODE. */
struct inode *
inode_reopen (struct inode *inode)
{
  if (inode != NULL)
    {
      lock_acquire (&inode->metadata_lock);
      inode->open_cnt++;
      lock_release (&inode->metadata_lock);
    }
  return inode;
}

/* Returns INODE's inode number. */
block_sector_t
inode_get_inumber (struct inode *inode)
{
  return inode->sector;
}

/* Closes INODE and writes it to disk.
   If this was the last reference to INODE, frees its memory.
   If INODE was also a removed inode, frees its blocks. */
void
inode_close (struct inode *inode) 
{
  /* Ignore null pointer. */
  if (inode == NULL)
    return;
  lock_acquire (&inode_list_lock);
  /* Release resources if this was the last opener. */
  if (--inode->open_cnt == 0)
    {
      /* Remove from inode list and release lock. */      
      list_remove (&inode->elem);
     
      /* Deallocate blocks if removed. */
      if (inode->removed)
        {
          free_map_release (inode->sector, 1);
          block_sector_t block_counts [4], i;
          get_all_block_counts (inode->data.length, block_counts);
          for (i = 0; i < block_counts [0]; i++)
            free_map_release (inode->data.direct [i], 1);
          
          if (block_counts [1] != 0)
            deallocate_singly_indirect (block_counts [1], 0, inode->data.singly_indirect);
          if (block_counts [2] != 0)
            deallocate_doubly_indirect (block_counts [2], block_counts [3], inode->data.doubly_indirect);
        }
      free (inode); 
    }
  lock_release (&inode_list_lock);
}

/* Marks INODE to be deleted when it is closed by the last caller who
   has it open. */
void
inode_remove (struct inode *inode) 
{
  ASSERT (inode != NULL);
  lock_acquire (&inode->metadata_lock);
  inode->removed = true;
  lock_release (&inode->metadata_lock);
}

/* Returns true if inode is marked as removed */
bool
inode_is_removed (struct inode *inode)
{
  lock_acquire (&inode->metadata_lock);
  bool removed = inode->removed;
  lock_release (&inode->metadata_lock);
  return removed;
}

/* Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
   Returns the number of bytes actually read, which may be less
   than SIZE if an error occurs or end of file is reached. */
off_t
inode_read_at (struct inode *inode, void *buffer_, off_t size, off_t offset) 
{
  uint8_t *buffer = buffer_;
  off_t bytes_read = 0;
  uint8_t *bounce = NULL;

  inode_data_read_enter (inode);
  while (size > 0) 
    {
      /* Disk sector to read, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually copy out of this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {
          /* Read full sector directly into caller's buffer. */
          block_read_wrapper (fs_device, sector_idx, buffer + bytes_read);
        }
      else 
        {
          /* Read sector into bounce buffer, then partially copy
             into caller's buffer. */
          if (bounce == NULL) 
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }
          block_read_wrapper (fs_device, sector_idx, bounce);
          memcpy (buffer + bytes_read, bounce + sector_ofs, chunk_size);
        }
      
      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_read += chunk_size;
    }
  free (bounce);
  inode_data_read_exit (inode);
  return bytes_read;
}

/* Extends disk_inode present at inode_sector to the given length */
bool inode_extend (struct inode_disk *disk_inode, block_sector_t inode_sector, off_t length)
{
  ASSERT (disk_inode->length <= length);
  if (disk_inode->length == length)
    {
      if (length == 0)
        block_write_wrapper (fs_device, inode_sector, (uint8_t *)disk_inode);
      return true;
    }
  bool success = false;
  block_sector_t counts_before [4], counts_after [4], i;
  get_all_block_counts (disk_inode->length, counts_before);
  get_all_block_counts (length, counts_after);
  
  for (i = counts_before [0]; i < counts_after [0]; i++)
    {
      if (!free_map_allocate (1, &disk_inode->direct [i]))
        {
          break;
        }
      block_write_wrapper (fs_device, disk_inode->direct [i], (uint8_t *)zeros);
    }
  if (i != counts_after [0])
    {
      if (i != counts_before [0])
        {
          i--;
          while (1)
            {
              free_map_release (disk_inode->direct [i], 1);
              if (i == counts_before [0])
                break;
              i--;
            }
        }
    }
  else
    {

      if (counts_before [1] == counts_after [1] 
         || allocate_singly_indirect (counts_before [1], counts_after [1], &disk_inode->singly_indirect))
        {
          if ((counts_before [2] == counts_after [2] && counts_before [3] == counts_after [3]) 
         || allocate_doubly_indirect (counts_before [2], counts_before [3], counts_after [2], counts_after [3], &disk_inode->doubly_indirect))
            {
              success = true;
            }
        }


    }
    
  if (success)
    {
      disk_inode->length = length;
      block_write_wrapper (fs_device, inode_sector, (uint8_t *)disk_inode);
    }
  return success;
}
/* Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
   Returns the number of bytes actually written, which may be
   less than SIZE if end of file is reached or an error occurs.
   (Normally a write at end of file would extend the inode, but
   growth is not yet implemented.) */
off_t
inode_write_at (struct inode *inode, const void *buffer_, off_t size,
                off_t offset) 
{
  const uint8_t *buffer = buffer_;
  off_t bytes_written = 0;
  uint8_t *bounce = NULL;
  
  inode_data_write_enter (inode);
  
  if (inode->deny_write_cnt)
    {
      inode_data_write_exit (inode); 
      return 0;
    }

  off_t new_length = offset + size;
  lock_acquire (&inode->metadata_lock);
  if (new_length > inode->data.length)
  /* if extension fails, the length of file remains same as old one */
    inode_extend (&inode->data, inode->sector, new_length);
  lock_release (&inode->metadata_lock);
  
  while (size > 0) 
    {
      /* Sector to write, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually write into this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {
          /* Write full sector directly to disk. */
          block_write_wrapper (fs_device, sector_idx, buffer + bytes_written);
        }
      else 
        {
          /* We need a bounce buffer. */
          if (bounce == NULL) 
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }

          /* If the sector contains data before or after the chunk
             we're writing, then we need to read in the sector
             first.  Otherwise we start with a sector of all zeros. */
          if (sector_ofs > 0 || chunk_size < sector_left) 
            block_read_wrapper (fs_device, sector_idx, bounce);
          else
            memset (bounce, 0, BLOCK_SECTOR_SIZE);
          memcpy (bounce + sector_ofs, buffer + bytes_written, chunk_size);
          block_write_wrapper (fs_device, sector_idx, bounce);
        }

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_written += chunk_size;
    }
  free (bounce);

  inode_data_write_exit (inode);
  
  return bytes_written;
}

/* Disables writes to INODE.
   May be called at most once per inode opener. */
void
inode_deny_write (struct inode *inode) 
{
  lock_acquire (&inode->metadata_lock);
  inode->deny_write_cnt++;
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
  lock_release (&inode->metadata_lock);
}

/* Re-enables writes to INODE.
   Must be called once by each inode opener who has called
   inode_deny_write() on the inode, before closing the inode. */
void
inode_allow_write (struct inode *inode) 
{
  lock_acquire (&inode->metadata_lock);
  ASSERT (inode->deny_write_cnt > 0);
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
  inode->deny_write_cnt--;
  lock_release (&inode->metadata_lock);
}

/* Returns the length, in bytes, of INODE's data. */
off_t
inode_length (struct inode *inode)
{
  lock_acquire (&inode->metadata_lock);
  off_t length = inode->data.length;
  lock_release (&inode->metadata_lock);
  return length;
}

void
inode_lock_dir (struct inode *inode)
{
  ASSERT (inode_is_dir (inode));
  lock_acquire (&inode->dir_lock);
}

void
inode_unlock_dir (struct inode *inode)
{
  ASSERT (inode_is_dir (inode));
  lock_release (&inode->dir_lock);
}
