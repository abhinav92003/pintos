#ifndef THREADS_THREAD_H
#define THREADS_THREAD_H

#include <debug.h>
#include <list.h>
#include <stdint.h>
#include "lib/kernel/fixed_point.h"
#include "lib/kernel/hash.h"
#include "threads/synch.h"
#include "filesys/directory.h"

/* States in a thread's life cycle. */
enum thread_status
  {
    THREAD_RUNNING,     /* Running thread. */
    THREAD_READY,       /* Not running but ready to run. */
    THREAD_BLOCKED,     /* Waiting for an event to trigger. */
    THREAD_DYING        /* About to be destroyed. */
  };

/* Thread identifier type.
   You can redefine this to whatever type you like. */
typedef int tid_t;
#define TID_ERROR ((tid_t) -1)          /* Error value for tid_t. */

/* Thread priorities. */
#define PRI_MIN 0                       /* Lowest priority. */
#define PRI_DEFAULT 31                  /* Default priority. */
#define PRI_MAX 63                      /* Highest priority. */

/* A kernel thread or user process.

   Each thread structure is stored in its own 4 kB page.  The
   thread structure itself sits at the very bottom of the page
   (at offset 0).  The rest of the page is reserved for the
   thread's kernel stack, which grows downward from the top of
   the page (at offset 4 kB).  Here's an illustration:

        4 kB +---------------------------------+
             |          kernel stack           |
             |                |                |
             |                |                |
             |                V                |
             |         grows downward          |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             |                                 |
             +---------------------------------+
             |              magic              |
             |                :                |
             |                :                |
             |               name              |
             |              status             |
        0 kB +---------------------------------+

   The upshot of this is twofold:

      1. First, `struct thread' must not be allowed to grow too
         big.  If it does, then there will not be enough room for
         the kernel stack.  Our base `struct thread' is only a
         few bytes in size.  It probably should stay well under 1
         kB.

      2. Second, kernel stacks must not be allowed to grow too
         large.  If a stack overflows, it will corrupt the thread
         state.  Thus, kernel functions should not allocate large
         structures or arrays as non-static local variables.  Use
         dynamic allocation with malloc() or palloc_get_page()
         instead.

   The first symptom of either of these problems will probably be
   an assertion failure in thread_current(), which checks that
   the `magic' member of the running thread's `struct thread' is
   set to THREAD_MAGIC.  Stack overflow will normally change this
   value, triggering the assertion. */
/* The `elem' member has a dual purpose.  It can be an element in
   the run queue (thread.c), or it can be an element in a
   semaphore wait list (synch.c).  It can be used these two ways
   only because they are mutually exclusive: only a thread in the
   ready state is on the run queue, whereas only a thread in the
   blocked state is on a semaphore wait list. */
struct thread
  {
    /* Owned by thread.c. */
    tid_t tid;                          /* Thread identifier. */
    enum thread_status status;          /* Thread state. */
    char name[16];                      /* Name (for debugging purposes). */
    uint8_t *stack;                     /* Saved stack pointer. */
    int priority;                       /* Effective Priority of thread */
    int base_priority;                  /* Base priority of thread
                                           base_priority == -1 if no priority 
                                           donation not in effect. Otherwise, 
                                           base_priority contains the actual 
                                           priority */
    struct thread *lock_waiting_for;    /* If current thread is waiting for a  
                                           lock, lock_waiting_for is the thread
                                           currently holding that lock. 
                                           Used to implement priority donation */
    int nice;                           /* 'nice' value for thread */
    fixed_point recent_cpu;             /* recent_cpu for thread */
              
    int64_t wakeup_time;                /* if thread is sleeping, wakeup_time is
                                          when it has to be woken up
                                           otherwise it is -1 */
        
    tid_t parent;                       /* TID of Parent thread*/
                                               
    struct list_elem sleepelem;         /* List element for list of sleeping threads */
    
    /* Shared between thread.c and synch.c  */        
    struct list locks_held;             /* List of locks the current thread holds */
    
    struct list_elem allelem;           /* List element for all threads list. */
    
    /* Shared between thread.c and synch.c. */
    struct list_elem elem;              /* List element. */
    
    struct dir *pwd;                    /* Current directory of process */    
    
#ifdef USERPROG
    /* Owned by userprog/process.c. */
    uint32_t *pagedir;                  /* Page directory. */
    
    struct file* executable;            /* executable of the file that the thread
                                           is executing */
                                           
    struct semaphore sema_child;        /* Used for synchronisation with child 
                                           thread */
    
    tid_t process_waiting_for;          /* if process has called process_wait (),
                                           or is waiting for a child process to
                                           load, then process_waiting_for
                                           contains the tid_t its waiting for,
                                           otherwise it is -1 */
              
    struct list open_files;             /* List of open files with file 
                                           descriptors */
    
    struct list malloc_mem;             /* List of memory that have been 
                                           allocated using malloc */
    
    struct hash spt;                    /* Supplemental page table */
    
    struct lock spt_lock;               /* Lock for accessing supplemental
                                           page table */
    
    struct list open_mmap;              /* List of files currently Mmap'd */
                
#endif
    
    
    /* Owned by thread.c. */
    unsigned magic;                     /* Detects stack overflow. */
  };

#ifdef USERPROG
/* Structure to store exit status of process that has completed execution.
   This enables a parent process to retrieve the exit status by calling 
   process_wait (). */
struct dead_thread
  {
    tid_t tid;                          /* Thread identifier. */
    int exit_status;                    /* Exit status of thread. */
    tid_t parent;                       /* Thread identifier of parent */
    struct list_elem elem;              /* List element for list of dead
                                           threads */
  };
  

/* Lock to allow only one process's page fault to be serviced at a time.
   For debugging purposes */
//struct lock pf_lock;
#endif




/* If false (default), use round-robin scheduler.
   If true, use multi-level feedback queue scheduler.
   Controlled by kernel command-line option "-o mlfqs". */
extern bool thread_mlfqs;

void thread_init (void);
void thread_start (void);

void thread_tick (void);
void thread_print_stats (void);

typedef void thread_func (void *aux);
tid_t thread_create (const char *name, int priority, thread_func *, void *);

void thread_block (void);
void thread_unblock (struct thread *);

struct thread *thread_current (void);
tid_t thread_tid (void);
const char *thread_name (void);

void thread_exit (void) NO_RETURN;
void thread_yield (void);

void thread_sleep_till (int64_t);

/* Performs some operation on thread t, given auxiliary data AUX. */
typedef void thread_action_func (struct thread *t, void *aux);
void thread_foreach (thread_action_func *, void *);

int thread_get_priority (void);
void thread_set_priority (int);

int thread_get_nice (void);
void thread_set_nice (int);
int thread_get_recent_cpu (void);
int thread_get_load_avg (void);

struct dir * thread_get_pwd (void);
void thread_set_pwd (struct dir *);
void thread_close_pwd (void);

void thread_update_donated_priority (void);
bool wakeup_time_less (const struct list_elem *a, const struct list_elem *b, void *aux);
void thread_change_ready_list (struct thread *);

tid_t get_parent_tid (tid_t);

#ifdef USERPROG  
/* dead_list is a list of struct dead_thread. When a process exits, it is added
   to dead_list.
   After process_wait () has been called for a process, its struct dead_thread
   is removed from dead_list */
struct list dead_list;

struct thread* get_thread_alive (tid_t);
struct dead_thread* get_thread_dead (tid_t);
#endif
#endif /* threads/thread.h */
