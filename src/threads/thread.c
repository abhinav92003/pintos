#include "threads/thread.h"
#include "threads/malloc.h"
#include "devices/timer.h"
#include <debug.h>
#include <stddef.h>
#include <random.h>
#include <stdio.h>
#include <string.h>
#include "threads/flags.h"
#include "threads/interrupt.h"
#include "threads/intr-stubs.h"
#include "threads/palloc.h"
#include "threads/switch.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#include "vm/page_frame.h"
#include "vm/swap.h"
#include "filesys/inode.h"
#ifdef USERPROG
#include "userprog/process.h"
#endif

/* Random value for struct thread's `magic' member.
   Used to detect stack overflow.  See the big comment at the top
   of thread.h for details. */
#define THREAD_MAGIC 0xcd6abf4b
#define FP_OP_DIG_AFTER_POINT 14
#define NICE_MIN (-20)
#define NICE_MAX 20
/* List of processes in THREAD_READY state, that is, processes
   that are ready to run but not actually running. */
static struct list ready_list [PRI_MAX - PRI_MIN +1];

/* List of all processes.  Processes are added to this list
   when they are first scheduled and removed when they exit. */
static struct list all_list;

/* List of all sleeping threads and when to wake each of them up.
   Elements sorted in order of wake time */
static struct list sleep_list;   
   
/* Idle thread. */
static struct thread *idle_thread;

/* Initial thread, the thread running init.c:main(). */
static struct thread *initial_thread;

/* Lock used by allocate_tid(). */
static struct lock tid_lock;

static fixed_point load_avg;
/* Stack frame for kernel_thread(). */
struct kernel_thread_frame 
  {
    void *eip;                  /* Return address. */
    thread_func *function;      /* Function to call. */
    void *aux;                  /* Auxiliary data for function. */
  };

/* Statistics. */
static long long idle_ticks;    /* # of timer ticks spent idle. */
static long long kernel_ticks;  /* # of timer ticks in kernel threads. */
static long long user_ticks;    /* # of timer ticks in user programs. */

/* Scheduling. */
#define TIME_SLICE 4            /* # of timer ticks to give each thread. */
static unsigned thread_ticks;   /* # of timer ticks since last yield. */

/* If false (default), use round-robin scheduler.
   If true, use multi-level feedback queue scheduler.
   Controlled by kernel command-line option "-o mlfqs". */
bool thread_mlfqs;

static void kernel_thread (thread_func *, void *aux);

static void idle (void *aux UNUSED);
static struct thread *running_thread (void);
static struct thread *next_thread_to_run (void);
static void init_thread (struct thread *, const char *name, int priority, fixed_point recent_cpu, int nice);
static bool is_thread (struct thread *) UNUSED;
static void *alloc_frame (struct thread *, size_t size);
static void schedule (void);
void thread_schedule_tail (struct thread *prev);
static tid_t allocate_tid (void);

static int find_priority_highest_thread (void);
static void thread_update_priority (struct thread *);
static void thread_update_recent_cpu (struct thread *, void *);
static int get_count_ready_running_threads (void);
static void update_load_avg (void);


/* Initializes the threading system by transforming the code
   that's currently running into a thread.  This can't work in
   general and it is possible in this case only because loader.S
   was careful to put the bottom of the stack at a page boundary.

   Also initializes the run queue and the tid lock.

   After calling this function, be sure to initialize the page
   allocator before trying to create any threads with
   thread_create().

   It is not safe to call thread_current() until this function
   finishes. */
void
thread_init (void) 
{
  ASSERT (intr_get_level () == INTR_OFF);
  
  lock_init (&tid_lock);
  int i;
  for (i = PRI_MIN; i <= PRI_MAX; i++)
    {
      list_init (&ready_list [i - PRI_MIN]);
    }
  
  list_init (&all_list);
  list_init (&sleep_list);
  
  #ifdef USERPROG
  list_init (&dead_list);   
  
  swap_init ();
  frame_init ();
    
  #endif

  load_avg = 0;
  /* Set up a thread structure for the running thread. */
  initial_thread = running_thread ();
  init_thread (initial_thread, "main", PRI_DEFAULT, 0, 0);
  initial_thread->status = THREAD_RUNNING;
  initial_thread->tid = allocate_tid ();


}

/* Returns the number of threads with status THREAD_READY or THREAD_RUNNING */
static int 
get_count_ready_running_threads ()
{
  ASSERT (intr_context ()); // For now, this function is called only in intr_context ()
  int i;
  int count = 0;
  for (i = PRI_MIN; i <= PRI_MAX; i++)
      count += list_size (&ready_list [i - PRI_MIN]);
  if (thread_current () != idle_thread)
    count++;
  return count;
}


/* Starts preemptive thread scheduling by enabling interrupts.
   Also creates the idle thread. */
void
thread_start (void) 
{
  /* Create the idle thread. */
  struct semaphore idle_started;
  sema_init (&idle_started, 0);
  thread_create ("idle", PRI_MIN, idle, &idle_started);

  /* Start preemptive thread scheduling. */
  intr_enable ();

  /* Wait for the idle thread to initialize idle_thread. */
  sema_down (&idle_started);
}

/* Called by the timer interrupt handler at each timer tick.
   Thus, this function runs in an external interrupt context. */
void
thread_tick (void) 
{
  ASSERT (intr_context ());
  struct thread *t = thread_current ();

  /* Update statistics. */
  if (t == idle_thread)
    idle_ticks++;
#ifdef USERPROG
  else if (t->pagedir != NULL)
    user_ticks++;
#endif
  else
    kernel_ticks++;
  
  /* Scan sleep_list and wake up threads whose wakeup_time has passed */
  struct list_elem *e;
  int64_t cur_ticks = timer_ticks ();
  for (e = list_begin (&sleep_list); e != list_end (&sleep_list) ; )
    {    
      t = list_entry (e, struct thread, sleepelem);     
      if (t->wakeup_time > cur_ticks)
        break;  
      t->wakeup_time = -1;
      e = list_remove (e);
      thread_unblock (t); 
    }
   t = thread_current ();
   if (thread_mlfqs)
   {
     
     if (t != idle_thread)
      t->recent_cpu = add_fp_int (t->recent_cpu, 1, FP_OP_DIG_AFTER_POINT);
     if (timer_ticks () % TIMER_FREQ == 0)
      {
        update_load_avg ();
        thread_foreach (&thread_update_recent_cpu, NULL);
      }
      
      if (timer_ticks () % 4 == 0)
      {
        int i;
        for (i = PRI_MIN; i <= PRI_MAX; i++)
            list_init (&ready_list [i - PRI_MIN]);

        /* Cannot use thread_foreach () for the following because the above loop
          for list_init () is necessary. Putting the below in a function for
          thread_foreach () may lead to confusion */
        struct list_elem *e;
        for (e = list_begin (&all_list); e != list_end (&all_list);
             e = list_next (e))
          {
            struct thread *t = list_entry (e, struct thread, allelem);
            if (t == idle_thread) // idle thread does not need to be put in ready queue
              continue;           
            thread_update_priority (t);
            if (t->status == THREAD_READY)
              list_push_back (&ready_list [t->priority - PRI_MIN], &t->elem);
          }        
                  
        t = thread_current ();
        if (t != idle_thread)
        {
          thread_update_priority (t);
          if (thread_get_priority () < find_priority_highest_thread ())
            intr_yield_on_return ();
        }       
      }
   }
      
  /* Enforce preemption. */
  if (++thread_ticks >= TIME_SLICE)
    intr_yield_on_return ();
}

/* Prints thread statistics. */
void
thread_print_stats (void) 
{
  printf ("Thread: %lld idle ticks, %lld kernel ticks, %lld user ticks\n",
          idle_ticks, kernel_ticks, user_ticks);
}

/* Creates a new kernel thread named NAME with the given initial
   PRIORITY, which executes FUNCTION passing AUX as the argument,
   and adds it to the ready queue.  Returns the thread identifier
   for the new thread, or TID_ERROR if creation fails.

   If thread_start() has been called, then the new thread may be
   scheduled before thread_create() returns.  It could even exit
   before thread_create() returns.  Contrariwise, the original
   thread may run for any amount of time before the new thread is
   scheduled.  Use a semaphore or some other form of
   synchronization if you need to ensure ordering.

   The code provided sets the new thread's `priority' member to
   PRIORITY, but no actual priority scheduling is implemented.
   Priority scheduling is the goal of Problem 1-3. */
tid_t
thread_create (const char *name, int priority,
               thread_func *function, void *aux) 
{
  struct thread *t ,*cur = thread_current ();
  struct kernel_thread_frame *kf;
  struct switch_entry_frame *ef;
  struct switch_threads_frame *sf;
  tid_t tid;

  ASSERT (function != NULL);
  /* Allocate thread. */
  t = palloc_get_page (PAL_ZERO);
  if (t == NULL)
    return TID_ERROR;

  /* Initialize thread. */
  init_thread (t, name, priority, cur->recent_cpu, cur->nice);
  t->parent = cur->tid;
  tid = t->tid = allocate_tid ();
  
  /* Stack frame for kernel_thread(). */
  kf = alloc_frame (t, sizeof *kf);
  kf->eip = NULL;
  kf->function = function;
  kf->aux = aux;

  /* Stack frame for switch_entry(). */
  ef = alloc_frame (t, sizeof *ef);
  ef->eip = (void (*) (void)) kernel_thread;

  /* Stack frame for switch_threads(). */
  sf = alloc_frame (t, sizeof *sf);
  sf->eip = switch_entry;
  sf->ebp = 0;

  /* Add to run queue. */
  thread_unblock (t);
  
  return tid;
}

/* Puts the current thread to sleep.  It will not be scheduled
   again until awoken by thread_unblock().

   This function must be called with interrupts turned off.  It
   is usually a better idea to use one of the synchronization
   primitives in synch.h. */
void
thread_block (void) 
{
  ASSERT (!intr_context ());
  ASSERT (intr_get_level () == INTR_OFF);
  
  struct thread *t = thread_current ();
  list_remove (&t->allelem);
  list_push_back (&all_list, &t->allelem);
  t->status = THREAD_BLOCKED;
  schedule ();
}

/* Transitions a blocked thread T to the ready-to-run state.
   This is an error if T is not blocked.  (Use thread_yield() to
   make the running thread ready.)

   This function does not preempt the running thread.  This can
   be important: if the caller had disabled interrupts itself,
   it may expect that it can atomically unblock a thread and
   update other data. */
void
thread_unblock (struct thread *t) 
{
  enum intr_level old_level;

  ASSERT (is_thread (t));
  old_level = intr_disable ();
  int highest_priority = find_priority_highest_thread ();
  if (thread_get_priority () > highest_priority)
    highest_priority = thread_get_priority ();
  
  ASSERT (t->status == THREAD_BLOCKED);
  list_push_back (&ready_list [t->priority - PRI_MIN], &t->elem);
  t->status = THREAD_READY;
  if (highest_priority < t->priority)
  { 
    if (intr_context ()) 
      intr_yield_on_return ();
    else if (thread_current () != idle_thread )    
      thread_yield (); 
  }
  intr_set_level (old_level);
}

/* Returns the name of the running thread. */
const char *
thread_name (void) 
{
  return thread_current ()->name;
}

/* Returns the running thread.
   This is running_thread() plus a couple of sanity checks.
   See the big comment at the top of thread.h for details. */
struct thread *
thread_current (void) 
{
  struct thread *t = running_thread ();
  
  /* Make sure T is really a thread.
     If either of these assertions fire, then your thread may
     have overflowed its stack.  Each thread has less than 4 kB
     of stack, so a few big automatic arrays or moderate
     recursion can cause stack overflow. */
  ASSERT (is_thread (t));
  ASSERT (t->status == THREAD_RUNNING);

  return t;
}

struct dir*
thread_get_pwd ()
{
  struct thread *cur = thread_current ();
  if (cur->pwd == NULL)
    cur->pwd = dir_open_root ();
 
  return dir_reopen (cur->pwd);
}

void
thread_close_pwd ()
{
  dir_close (thread_current ()->pwd);
  thread_current ()->pwd = NULL; /* Next call to thread_get_pwd () will set it
                                    to ROOT */
}

void
thread_set_pwd (struct dir *dir)
{
  dir_close (thread_current ()->pwd);
  thread_current ()->pwd = dir_reopen (dir);
}

/* Returns the running thread's tid. */
tid_t
thread_tid (void) 
{
  return thread_current ()->tid;
}

#ifdef USERPROG

/* Returns struct thread * of thread tid. If it isn't present in all_list 
   (i.e. it isn't alive), NULL is returned */
struct thread*
get_thread_alive (tid_t tid)
{
  struct list_elem *e;
  for (e = list_begin (&all_list); e != list_end (&all_list);
      e = list_next (e))
    {
      struct thread *t = list_entry (e, struct thread, allelem);
      if (t->tid == tid)
        return t;
    }
  return NULL;
}

/* Returns struct thread * of thread tid. If it isn't present in dead_list 
   (i.e. it isn't dead or process_wait (tid) has already been called), 
   NULL is returned */
struct dead_thread*
get_thread_dead (tid_t tid)
{
  struct list_elem *e;
  for (e = list_begin (&dead_list); e != list_end (&dead_list);
      e = list_next (e))
    {
      struct dead_thread *t = list_entry (e, struct dead_thread, elem);
      if (t->tid == tid)
        return t;
    }
  return NULL;
}
#endif

/* Returns thread identifier of parent of thread tid. 
   If the thread tid is not present in either all_list or dead_list, returns -1 */
tid_t get_parent_tid (tid_t tid)
{
   struct list_elem *e;
   struct thread *t;
   for (e = list_begin (&all_list); e != list_end (&all_list);
      e = list_next (e))
    {
      t = list_entry (e, struct thread, allelem);
      if (t->tid == tid)
        return t->parent;
    }
    #ifdef USERPROG
    struct dead_thread *td;
    for (e = list_begin (&dead_list); e != list_end (&dead_list);
          e = list_next (e))
      {
        td = list_entry (e, struct dead_thread, elem);
        if (td->tid == tid)
          return td->parent;
      }
    #endif
    return -1;
}

/* Deschedules the current thread and destroys it.  Never
   returns to the caller. */
void
thread_exit (void) 
{
  ASSERT (!intr_context ());
  struct thread *cur = thread_current ();
    
#ifdef USERPROG
   process_exit ();
#endif
  
  /* Release the locks held by thread.*/
  struct list_elem *e, *e2;
  for (e = list_begin (&cur->locks_held); e != list_end (&cur->locks_held);)
    {
      e2 = e;
      e = list_next (e);
      lock_release (list_entry (e2, struct lock, elem));
    }
  thread_close_pwd ();
  intr_disable ();
    
  /* Remove thread from all threads list, set our status to dying,
     and schedule another process.  That process will destroy us
     when it calls thread_schedule_tail(). */
  list_remove (&cur->allelem);
  cur->status = THREAD_DYING;
  schedule ();
  NOT_REACHED ();
}
 
/* Yields the CPU.  The current thread is not put to sleep and
   may be scheduled again immediately at the scheduler's whim. */
void
thread_yield (void) 
{
  struct thread *cur = thread_current ();
  enum intr_level old_level;
  ASSERT (!intr_context ());
  old_level = intr_disable ();
  list_remove (&cur->allelem);
  list_push_back (&all_list, &cur->allelem);
  if (cur != idle_thread) 
    list_push_back (&ready_list [thread_get_priority () - PRI_MIN], &cur->elem);
  cur->status = THREAD_READY;
  schedule ();
  intr_set_level (old_level);
}

/* Comparator function to insert in sleep_list in non-decreasing order of 
   waking up time of sleeping threads */
bool wakeup_time_less (const struct list_elem *a,
                             const struct list_elem *b,
                             void *aux UNUSED)
{
  struct thread *a_thread = list_entry (a, struct thread, sleepelem);
  struct thread *b_thread = list_entry (b, struct thread, sleepelem);
  if (a_thread->wakeup_time < b_thread->wakeup_time)
    return true;
  return false;
}
/* Adds the current thread to the linked list of sleeping threads (sleep_list) 
   and then blocks the thread. Insertion is done so that sleep_list has sleeping
   threads in non-decreasing order of wakeup time */
void
thread_sleep_till (int64_t sleep_till)
{
  ASSERT (!intr_context ());
  struct thread *t = thread_current ();
  t->wakeup_time = sleep_till;
  int old_level = intr_disable ();
  list_insert_ordered (&sleep_list, &t->sleepelem, &wakeup_time_less, NULL);
  thread_block();
  intr_set_level (old_level);
}

/* Finds and returns the priority of highest priority thread in ready list */
static int
find_priority_highest_thread ()
  {
    int highest_priority;
    for (highest_priority = PRI_MAX; highest_priority >= PRI_MIN; highest_priority--)
      if (!list_empty (&ready_list [highest_priority - PRI_MIN]))
        break;
    return highest_priority; 
  }

/* Removes t from its earlier ready list and puts in new ready list */
void
thread_change_ready_list (struct thread *t)
  {
    ASSERT (intr_get_level () == INTR_OFF);
    if (t == idle_thread)
      return;
    list_remove (&t->elem);
    list_push_back (&ready_list [t->priority - PRI_MIN], &t->elem);
  }
  
/* Invoke function 'func' on all threads, passing along 'aux'.
   This function must be called with interrupts off. */
void
thread_foreach (thread_action_func *func, void *aux)
{
  struct list_elem *e;

  ASSERT (intr_get_level () == INTR_OFF);

  for (e = list_begin (&all_list); e != list_end (&all_list);
       e = list_next (e))
    {
      struct thread *t = list_entry (e, struct thread, allelem);
      func (t, aux);
    }
}

/* Sets the current thread's priority to NEW_PRIORITY. */
void
thread_set_priority (int new_priority) 
{
  if (thread_mlfqs)
    return;
  struct thread *cur = thread_current ();
  if (cur->base_priority == PRI_MIN - 1)
    cur->priority = new_priority;
  else
    {   
      cur->base_priority = new_priority;
      thread_update_donated_priority();
    }
  enum intr_level old_level = intr_disable ();
  if (find_priority_highest_thread () > thread_get_priority ())
      thread_yield ();
  intr_set_level (old_level);
}

/* Updates the priority of current thread to the priority of highest priority thread
   waiting on any of the locks currently acquired by it.
   If base_priority is higher than the above, priority is set as the base_priority. */
void
thread_update_donated_priority()
{
  ASSERT (!thread_mlfqs);
  enum intr_level old_level = intr_disable ();
  struct thread *cur = thread_current ();
  if (list_empty (&cur->locks_held))
    {
      cur->priority = cur->base_priority;
      cur->base_priority = PRI_MIN - 1;
    }
  else
    {   
      struct list_elem *e;
      struct lock *l;
      struct thread *t,*cur = thread_current ();
      int hp = PRI_MIN - 1;
      
      for (e = list_begin (&cur->locks_held); e != list_end (&cur->locks_held); 
            e = list_next (e))
        {
          l = list_entry (e, struct lock, elem); 
          if (!list_empty (&(l->semaphore).waiters))
            {
              t = list_entry (list_max(&(l->semaphore).waiters, &thread_priority_less, NULL),
                               struct thread, elem);
              if (t->priority > hp)
                hp = t->priority;
            }
        }
        
        cur->priority = hp;
        if (cur->base_priority > cur->priority)
          cur->priority = cur->base_priority;
        
      }
    intr_set_level (old_level);
    return;
}


/* Returns the current thread's priority. */
int
thread_get_priority (void) 
{
  return thread_current ()->priority;
}

/* Sets the current thread's nice value to NICE. */
void
thread_set_nice (int nice) 
{
  ASSERT (thread_mlfqs);
  struct thread *t = thread_current ();
  ASSERT (NICE_MIN <= nice && nice <= NICE_MAX);
  t->nice = nice; 
  thread_update_priority (t);    
  enum intr_level old_level = intr_disable ();
  if (find_priority_highest_thread () > thread_get_priority ())
    thread_yield ();
  intr_set_level (old_level);
}

/* Computes and sets the new value of priority of thread t. The calculation is
  based on the current nice value of t. It does not put the thread in 
  appropriate ready list */
static void
thread_update_priority (struct thread *t)
{
  ASSERT (thread_mlfqs);
  fixed_point new_priority_fp = convert_int_to_fp (PRI_MAX, FP_OP_DIG_AFTER_POINT);
  fixed_point nice_fp = convert_int_to_fp (t->nice, FP_OP_DIG_AFTER_POINT);
  fixed_point recent_cpu_fp = t->recent_cpu;
  new_priority_fp = subtract_fp_fp (new_priority_fp, divide_fp_int (recent_cpu_fp, 4));
  new_priority_fp = subtract_fp_fp (new_priority_fp, multiply_fp_int (nice_fp, 2));
  int new_priority = convert_fp_to_int_truncate (new_priority_fp, FP_OP_DIG_AFTER_POINT);
  if (new_priority > PRI_MAX)
    new_priority = PRI_MAX;
  if (new_priority < PRI_MIN)
    new_priority = PRI_MIN;
  t->priority = new_priority;
}

/* Computes and sets the new value for recent_cpu of thread t */
static void
thread_update_recent_cpu (struct thread *t, void *aux UNUSED)
{
  ASSERT (thread_mlfqs);
  ASSERT (intr_context ());
  if (t == idle_thread) // idle thread's priority does  need to be recalculated
    return;
  fixed_point new_rc = multiply_fp_int (load_avg, 2);
  fixed_point temp = new_rc;
  temp = add_fp_int (temp, 1, FP_OP_DIG_AFTER_POINT);
  new_rc = divide_fp_fp (new_rc, temp, FP_OP_DIG_AFTER_POINT);
  new_rc = multiply_fp_fp (new_rc, t->recent_cpu, FP_OP_DIG_AFTER_POINT);
  new_rc = add_fp_int (new_rc, t->nice, FP_OP_DIG_AFTER_POINT);
  t->recent_cpu = new_rc;
  return;
}

/* Computes and sets the new value for load average of the system */
static void 
update_load_avg ()
{
  ASSERT (thread_mlfqs);
  ASSERT (intr_context ());
  fixed_point ratio1 = divide_fp_int (convert_int_to_fp (59, FP_OP_DIG_AFTER_POINT), 60);
  fixed_point ratio2 = divide_fp_int (convert_int_to_fp (1, FP_OP_DIG_AFTER_POINT), 60);
 
  load_avg = multiply_fp_fp (ratio1, load_avg, FP_OP_DIG_AFTER_POINT);
  load_avg = add_fp_fp (load_avg, multiply_fp_int (ratio2, get_count_ready_running_threads ()));
  return;
}

/* Returns the current thread's nice value. */
int
thread_get_nice (void) 
{
  ASSERT (thread_mlfqs);
  return thread_current ()->nice;
}

/* Returns 100 times the system load average. */
int
thread_get_load_avg (void) 
{
  ASSERT (thread_mlfqs);
  return convert_fp_to_int_nearest (multiply_fp_int (load_avg, 100), FP_OP_DIG_AFTER_POINT);
}

/* Returns 100 times the current thread's recent_cpu value. */
int
thread_get_recent_cpu (void) 
{
  ASSERT (thread_mlfqs);
  return convert_fp_to_int_nearest (multiply_fp_int (thread_current ()->recent_cpu, 100), FP_OP_DIG_AFTER_POINT);
}

/* Idle thread.  Executes when no other thread is ready to run.

   The idle thread is initially put on the ready list by
   thread_start().  It will be scheduled once initially, at which
   point it initializes idle_thread, "up"s the semaphore passed
   to it to enable thread_start() to continue, and immediately
   blocks.  After that, the idle thread never appears in the
   ready list.  It is returned by next_thread_to_run() as a
   special case when the ready list is empty. */
static void
idle (void *idle_started_ UNUSED) 
{
  struct semaphore *idle_started = idle_started_;
  idle_thread = thread_current ();
  sema_up (idle_started);

  for (;;) 
    {
      /* Let someone else run. */
      intr_disable ();
      thread_block ();

      /* Re-enable interrupts and wait for the next one.

         The `sti' instruction disables interrupts until the
         completion of the next instruction, so these two
         instructions are executed atomically.  This atomicity is
         important; otherwise, an interrupt could be handled
         between re-enabling interrupts and waiting for the next
         one to occur, wasting as much as one clock tick worth of
         time.

         See [IA32-v2a] "HLT", [IA32-v2b] "STI", and [IA32-v3a]
         7.11.1 "HLT Instruction". */
      asm volatile ("sti; hlt" : : : "memory");
    }
}

/* Function used as the basis for a kernel thread. */
static void
kernel_thread (thread_func *function, void *aux) 
{
  ASSERT (function != NULL);

  intr_enable ();       /* The scheduler runs with interrupts off. */
  function (aux);       /* Execute the thread function. */
  thread_exit ();       /* If function() returns, kill the thread. */
}

/* Returns the running thread. */
struct thread *
running_thread (void) 
{
  uint32_t *esp;

  /* Copy the CPU's stack pointer into `esp', and then round that
     down to the start of a page.  Because `struct thread' is
     always at the beginning of a page and the stack pointer is
     somewhere in the middle, this locates the curent thread. */
  asm ("mov %%esp, %0" : "=g" (esp));
  return pg_round_down (esp);
}

/* Returns true if T appears to point to a valid thread. */
static bool
is_thread (struct thread *t)
{
  return t != NULL && t->magic == THREAD_MAGIC;
}

/* Does basic initialization of T as a blocked thread named
   NAME. */
static void
init_thread (struct thread *t, const char *name, int priority, fixed_point recent_cpu, int nice)
{
  enum intr_level old_level;

  ASSERT (t != NULL);
  ASSERT (PRI_MIN <= priority && priority <= PRI_MAX);
  ASSERT (name != NULL);

  memset (t, 0, sizeof *t);
  t->status = THREAD_BLOCKED;
  strlcpy (t->name, name, sizeof t->name);
  t->stack = (uint8_t *) t + PGSIZE;
  t->parent = 0;
  
#ifdef USERPROG
   sema_init (&t->sema_child, 0);
   t->process_waiting_for = -1;
   list_init (&t->open_files);
   list_init (&t->malloc_mem);
   t->executable = NULL;
   t->pwd = NULL;
   //list_init (&t->spt);
  lock_init (&t->spt_lock);
  list_init (&t->open_mmap);
  
#endif
  
  if (!thread_mlfqs)
    {     
      t->lock_waiting_for = NULL;
      t->base_priority = PRI_MIN - 1;
      t->priority = priority;
    }
  else
    {
      t->nice = nice;
      t->recent_cpu = recent_cpu;    
      thread_update_priority (t);
    }    
  list_init (&t->locks_held);
  
  
  t->magic = THREAD_MAGIC;
  old_level = intr_disable ();
  list_push_back (&all_list, &t->allelem);
  intr_set_level (old_level);
}

/* Allocates a SIZE-byte frame at the top of thread T's stack and
   returns a pointer to the frame's base. */
static void *
alloc_frame (struct thread *t, size_t size) 
{
  /* Stack data is always allocated in word-size units. */
  ASSERT (is_thread (t));
  ASSERT (size % sizeof (uint32_t) == 0);

  t->stack -= size;
  return t->stack;
}

/* Chooses and returns the next thread to be scheduled.  Should
   return a thread from the run queue, unless the run queue is
   empty.  (If the running thread can continue running, then it
   will be in the run queue.)  If the run queue is empty, return
   idle_thread. */
static struct thread *
next_thread_to_run (void) 
{
  int highest_priority = find_priority_highest_thread();  
  if (highest_priority == PRI_MIN - 1)
    return idle_thread;
  else
    return list_entry (list_pop_front (&ready_list [highest_priority - PRI_MIN]), struct thread, elem);
}

/* Completes a thread switch by activating the new thread's page
   tables, and, if the previous thread is dying, destroying it.

   At this function's invocation, we just switched from thread
   PREV, the new thread is already running, and interrupts are
   still disabled.  This function is normally invoked by
   thread_schedule() as its final action before returning, but
   the first time a thread is scheduled it is called by
   switch_entry() (see switch.S).

   It's not safe to call printf() until the thread switch is
   complete.  In practice that means that printf()s should be
   added at the end of the function.

   After this function and its caller returns, the thread switch
   is complete. */
void
thread_schedule_tail (struct thread *prev)
{
  struct thread *cur = running_thread ();
  
  ASSERT (intr_get_level () == INTR_OFF);

  /* Mark us as running. */
  cur->status = THREAD_RUNNING;

  /* Start new time slice. */
  thread_ticks = 0;

#ifdef USERPROG
  /* Activate the new address space. */
  process_activate ();
#endif

  /* If the thread we switched from is dying, destroy its struct
     thread.  This must happen late so that thread_exit() doesn't
     pull out the rug under itself.  (We don't free
     initial_thread because its memory was not obtained via
     palloc().) */
  if (prev != NULL && prev->status == THREAD_DYING && prev != initial_thread) 
    {
      ASSERT (prev != cur);
      palloc_free_page (prev);
    }
}

/* Schedules a new process.  At entry, interrupts must be off and
   the running process's state must have been changed from
   running to some other state.  This function finds another
   thread to run and switches to it.

   It's not safe to call printf() until thread_schedule_tail()
   has completed. */
static void
schedule (void) 
{
  struct thread *cur = running_thread ();
  struct thread *next = next_thread_to_run ();
  struct thread *prev = NULL;

  ASSERT (intr_get_level () == INTR_OFF);
  ASSERT (cur->status != THREAD_RUNNING);
  ASSERT (is_thread (next));

  if (cur != next)
    prev = switch_threads (cur, next);
  thread_schedule_tail (prev);
}

/* Returns a tid to use for a new thread. */
static tid_t
allocate_tid (void) 
{
  static tid_t next_tid = 1;
  tid_t tid;

  lock_acquire (&tid_lock);
  tid = next_tid++;
  lock_release (&tid_lock);

  return tid;
}

/* Offset of `stack' member within `struct thread'.
   Used by switch.S, which can't figure it out on its own. */
uint32_t thread_stack_ofs = offsetof (struct thread, stack);
