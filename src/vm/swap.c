#include "devices/block.h"
#include "threads/malloc.h"
#include "threads/palloc.h"
#include "lib/string.h"
#include "threads/vaddr.h"
#include <stdio.h>
#include "vm/swap.h"
#include "userprog/process.h"

#define COUNT_SECTORS (PGSIZE / BLOCK_SECTOR_SIZE)

struct swapte
{
  block_sector_t from;    /* Should be a multiple of COUNT_SECTORS */
  struct thread *owner;   /* The thread that owns COUNT_SECTORS starting from
                             'from' */
  struct list_elem elem;  /* list_elem for swap_list */
};

struct list swap_list;    /* List that contains struct swapte for the sectors 
                             of swap block device that are currently occupied. */
struct lock swap_lock;    /* Lock to synchronize accesses to swap_list */

static block_sector_t get_free_sectors (struct thread *);

block_sector_t get_free_sectors (struct thread *owner)
{
  lock_acquire (&swap_lock);
  
  struct list_elem *e;
  struct swapte *cur;
  block_sector_t free_slot = 0;
  for (e = list_begin (&swap_list); e != list_end (&swap_list); 
        e = list_next (e))
    {
      cur = list_entry (e, struct swapte, elem);
      if (cur->from != free_slot)
        {
          break;
        }
      free_slot += COUNT_SECTORS;
    }
  struct swapte *new_slot = malloc (sizeof (struct swapte));
  new_slot->from = free_slot;
  new_slot->owner = owner;
  list_insert (e, &new_slot->elem);
  
  lock_release (&swap_lock);
 
  return free_slot;
}

block_sector_t write_frame_to_swap (void *frame, struct thread *owner)
{
  block_sector_t block_sectors = get_free_sectors (owner);
  int i;
  //lock_acquire (&swap_lock);
  for (i = 0; i < COUNT_SECTORS; i++)
    {
      block_write (block_get_role (BLOCK_SWAP), block_sectors + i, frame + BLOCK_SECTOR_SIZE * i);
    }
  //lock_release (&swap_lock);
  return block_sectors;
}

void read_page_from_swap (block_sector_t block_sectors, void *frame)
{
  int i;
  //lock_acquire (&swap_lock);
  for (i = 0; i < COUNT_SECTORS; i++)
    {
      block_read (block_get_role (BLOCK_SWAP), block_sectors + i, frame + BLOCK_SECTOR_SIZE * i); 
    }
  //lock_release (&swap_lock);
}

void remove_from_swap (block_sector_t block_sectors)
{
  
  lock_acquire (&swap_lock);
  struct list_elem *e;
  for (e = list_begin (&swap_list); e != list_end (&swap_list);
        e = list_next (e))
    {
      struct swapte *ste = list_entry (e, struct swapte, elem);
      if (ste->from == block_sectors)
        {
          list_remove (e);
          free (ste);
          break;
        }
    }
  lock_release (&swap_lock);
  return;
}

void thread_clean_swap ()
{
  lock_acquire (&swap_lock);
  
  struct thread *cur = thread_current ();
  struct list_elem *e;
  for (e = list_begin (&swap_list); e != list_end (&swap_list);)
    {
      struct swapte *ste = list_entry (e, struct swapte, elem);
      if (ste->owner == cur)
        {
          e = list_remove (e);
          free (ste);
        }
      else
        {
          e = list_next (e);
        }
    }
  lock_release (&swap_lock);
  return;
}

void swap_init ()
{
  list_init (&swap_list);
  lock_init (&swap_lock);
}

