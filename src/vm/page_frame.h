#ifndef VM_PAGE_FRAME_H
#define VM_PAGE_FRAME_H

#include <stdio.h>
#include "lib/kernel/hash.h"

struct spte;

#define PGSRC_FILE 1      // struct file*; read_bytes; offset; writable;
#define PGSRC_SWAP 2      // starting sector in swap block; ; ; ;
#define PGSRC_STACKSWAP 3 // starting sector in swap block; ; stack page number; ;
#define PGSRC_STACKZERO 4 // ; ; stack page number; writable;
#define PGSRC_ZERO 5      //  ; ;zero_bytes;writable;
#define PGSRC_EXECUTABLE 6// struct file*; read_bytes; offset; writable;

struct spte* add_spte (void *, char, uint32_t, uint32_t, uint32_t, uint32_t);
struct spte* find_spte (void *);
void remove_spte (struct spte *);
int get_count_stack_pages (void);
bool put_in_frame (struct spte *);
bool is_writable (struct spte *spte);
void frame_init (void);
void remove_all_spte (void);

unsigned spte_hash_func (const struct hash_elem *, void *);
bool spte_less_func (const struct hash_elem *,
                             const struct hash_elem *,
                             void *);
#endif /* vm/page_frame.h */
