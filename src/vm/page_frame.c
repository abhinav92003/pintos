#include "vm/page_frame.h"
#include "vm/swap.h"
#include <string.h>
#include "threads/vaddr.h"
#include "threads/thread.h"
#include "userprog/process.h"
#include "userprog/pagedir.h"
#include "filesys/file.h"
#include "userprog/syscall.h"
#include "threads/palloc.h"

/* Supplemental page table entry. An spte object can be present either in
   its thread's Supplemental Page hash table (thread->spt) or in frame_list */
struct spte
{
  void *page;                       /* Starting address of page */
  void *frame;                      /* Starting address of frame containing
                                       page, if any; otherwise NULL */
  struct thread *owner;             /* Thread that owns the spte */
  char src_type;                    /* Type of source for data of page;
                                       see page_frame.h for the various 
                                       possible sources */
  uint32_t src1, src2, src3, src4;  /* Source data */
  struct list_elem elem;            /* List elem for frame_list */
  struct hash_elem elemh;           /* Hash elem for thread's Supplemental
                                       Page Hash Table (thread->spt) */
};

struct list frame_list;             /* List of struct spte currently present in
                                       a frame */
struct lock frame_list_lock;        /* Lock for synchronising accesses to
                                       frame_list */


bool is_stack_spte (struct spte *);
bool install_page (void *, void *, bool);
void* get_frame (void);
void remove_from_frame (struct spte *, bool);

/* 
static 
bool page_less (const struct list_elem *, const struct list_elem *, void *);
static
bool page_less (const struct list_elem *a_elem, const struct list_elem *b_elem, 
                  void *aux UNUSED)
{
  struct spte *a = list_entry (a_elem, struct spte, elem);
  struct spte *b = list_entry (b_elem, struct spte, elem);
  if (a->page < b->page)
    return true;
  return false;
}
*/

void frame_init ()
{
  list_init (&frame_list);
  lock_init (&frame_list_lock);
}

/* hash function for struct spte; used for thread's supplemental page 
   hash table (thread->spt) */
unsigned spte_hash_func (const struct hash_elem *e, void *aux UNUSED)
  {
    struct spte *spte = hash_entry (e, struct spte, elemh);
    return (unsigned)pg_no (spte->page);
  }

/* hash less function for struct spte; used for thread->spt hash table */
bool spte_less_func (const struct hash_elem *a_elem,
                             const struct hash_elem *b_elem,
                             void *aux UNUSED)
  {
    struct spte *a = hash_entry (a_elem, struct spte, elemh);
    struct spte *b = hash_entry (b_elem, struct spte, elemh);
    if (pg_no (a->page) < pg_no (b->page))
      return true;
    return false;
  }

/* Constructs new spte and adds it in spt of running thread */
struct spte* add_spte (void *addr, char src_type, uint32_t src1, uint32_t src2, 
                        uint32_t src3, uint32_t src4)
{
  struct thread *cur = thread_current ();
  struct spte *new_spte = allocate_mem_malloc (sizeof (struct spte));
  new_spte->page = addr;
  new_spte->src_type = src_type;
  new_spte->src1 = src1;
  new_spte->src2 = src2;
  new_spte->src3 = src3;
  new_spte->src4 = src4; 
  new_spte->frame = NULL;
  new_spte->owner = cur;
  
  lock_acquire (&cur->spt_lock);
  hash_insert (&cur->spt, &new_spte->elemh);
  //list_push_back (&cur->spt, &new_spte->elem);
  lock_release (&cur->spt_lock);
  
  return new_spte;
}

/* Returns true if spte has information about a stack page */
bool is_stack_spte (struct spte *spte)
{
  if (spte->src_type == PGSRC_STACKZERO || spte->src_type == PGSRC_STACKSWAP)
    return true;
  return false;
}

/* Returns count of currently allocated stage spte */
int get_count_stack_pages ()
{
  //now that the list is sorted, scan from back instead to make it faster
  struct thread *cur = thread_current ();
  struct list_elem *e;
  int stack_pages = 0;
  
  lock_acquire (&cur->spt_lock);
  struct hash_iterator i;

  hash_first (&i, &cur->spt);
  while (hash_next (&i))
    {
      struct spte *spte = hash_entry (hash_cur (&i), struct spte, elemh);
      if (is_stack_spte (spte) && (int)spte->src3 > stack_pages)
        stack_pages = spte->src3;
    }
 /* for (e = list_begin (&cur->spt); e != list_end (&cur->spt);
        e = list_next (e))
    {
      struct spte *spte = list_entry (e, struct spte, elem);
      if (is_stack_spte (spte) && (int)spte->src3 > stack_pages)
        stack_pages = spte->src3;
    }  */
  lock_release (&cur->spt_lock);
  
  lock_acquire (&frame_list_lock);
  for (e = list_begin (&frame_list); e != list_end (&frame_list);
        e = list_next (e))
    {
      struct spte *spte = list_entry (e, struct spte, elem);
      if (spte->owner == cur)
        {
          if (is_stack_spte (spte) && (int)spte->src3 > stack_pages)
            stack_pages = spte->src3;
        }
    }
  lock_release (&frame_list_lock);
  return stack_pages;
}

/* If spte->page is present in a frame, removes it from the frame and frees the 
   frame. If save == true, then saves frame data to swap */
void remove_from_frame (struct spte *spte, bool save)
{
  ASSERT (lock_held_by_current_thread (&frame_list_lock));
  ASSERT (spte != NULL);
  ASSERT (spte->frame != NULL);
  
  lock_acquire (&spte->owner->spt_lock);
  pagedir_clear_page (spte->owner->pagedir, spte->page);
  if (save)
    {
      switch (spte->src_type)
      {
        case PGSRC_EXECUTABLE:
          // No writing to executable
          break;
        case PGSRC_FILE:
          if (pagedir_is_dirty (spte->owner->pagedir, spte->page))
            file_write_at ((struct file *)spte->src1, spte->frame, spte->src2,
                          spte->src3);
          break;
          
        case PGSRC_STACKZERO:
        case PGSRC_ZERO:
          ASSERT (false); 
          /* No need for saving zero pages. If zero pages were paged in, then
             their type would have been changed to PGSRC_SWAP or 
             PGSRC_STACKSWAP */ 
          break;
          
        case PGSRC_STACKSWAP:
        case PGSRC_SWAP:
          spte->src2 = 1;
          spte->src1 = write_frame_to_swap (spte->frame, spte->owner);
          
       //   printf("written %x to %d\n",spte->page,spte->src1);
          break;
        
      }
    }
  
  
  palloc_free_page (spte->frame);
  spte->frame = NULL;
  list_remove (&spte->elem);
  
  
  //list_push_back (&spte->owner->spt, &spte->elem);
  hash_insert (&spte->owner->spt, &spte->elemh);
  lock_release (&spte->owner->spt_lock);
  return;
}

/* Puts spte->page in a frame */
bool put_in_frame (struct spte *spte)
{
  ASSERT (spte != NULL);
  ASSERT (spte->frame == NULL);
  void *frame = get_frame ();
  if (frame == NULL)
    return false;
  
  bool installed = false;
  switch (spte->src_type)
    {
      case PGSRC_EXECUTABLE:
        file_read_at ((struct file *)spte->src1, frame, spte->src2,
                       spte->src3);
        memset (frame + spte->src2, 0, PGSIZE - spte->src2);
        installed = install_page (spte->page, frame, spte->src4); 
        if (spte->src3) // If it is a writable page of executable
          {
            spte->src_type = PGSRC_SWAP;
            spte->src2 = 0;
            spte->src3 = 0;
          }
        break;
        
      case PGSRC_FILE:        
        file_read_at ((struct file *)spte->src1, frame, spte->src2,
                       spte->src3);
        memset (frame + spte->src2, 0, PGSIZE - spte->src2);
        installed = install_page (spte->page, frame, spte->src4); 
        
        break;
      
      case PGSRC_ZERO:
        spte->src_type = PGSRC_SWAP;
        spte->src2 = 0;
        memset (frame, 0, PGSIZE);
        installed = install_page (spte->page, frame, spte->src4);
        break;
        
      case PGSRC_STACKZERO:
        spte->src_type = PGSRC_STACKSWAP;
        spte->src2 = 0;
        memset (frame, 0, PGSIZE);
        installed = install_page (spte->page, frame, spte->src4);
        break;
     
      case PGSRC_SWAP:
      case PGSRC_STACKSWAP:
        read_page_from_swap (spte->src1, frame);       
        remove_from_swap (spte->src1);
        spte->src2 = 0;
        spte->src1 = 0;

        installed = install_page (spte->page, frame, spte->src4);
        break;
    }
    
  if (!installed) 
    {
      palloc_free_page (frame);
      exit (-1);
    }
  pagedir_set_accessed (spte->owner->pagedir, spte->page, true);
  spte->frame = frame;
  
  
   
  lock_acquire (&frame_list_lock);
  lock_acquire (&spte->owner->spt_lock);
  //list_remove (&spte->elem);
  hash_delete (&spte->owner->spt, &spte->elemh);
  list_push_back (&frame_list, &spte->elem);
  
  lock_release (&spte->owner->spt_lock);
  lock_release (&frame_list_lock);
  return true;
}
 
/* Finds the spte that represents *page */
struct spte* find_spte (void *page)
{
  uintptr_t pgno = pg_no (page);
  struct thread *t = thread_current ();
  struct list_elem *e;
  lock_acquire (&t->spt_lock);
  struct spte *temp = allocate_mem_malloc (sizeof (struct spte));
  temp->page = page;
  struct spte *spte = NULL;
  struct hash_elem *spte_elemh = hash_find (&t->spt, &temp->elemh);
  if (spte_elemh != NULL)
    spte = hash_entry (spte_elemh, struct spte, elemh);
  /*
  for (e = list_begin (&t->spt); e != list_end (&t->spt); e = list_next (e))
    {
      struct spte *spte = list_entry (e, struct spte, elem);
      if (pg_no (spte->page) == pgno)
        {
          lock_release (&t->spt_lock);
          return spte;
        }
    }*/
  free_mem_malloc (temp);
  if (spte != NULL)
    {
      lock_release (&t->spt_lock);
      return spte;
    }
  
  lock_release (&t->spt_lock);
  
  lock_acquire (&frame_list_lock);
  for (e = list_begin (&frame_list); e != list_end (&frame_list);
        e = list_next (e))
    {
      struct spte *spte = list_entry (e, struct spte, elem);
      if (spte->owner == t)
        {
          if (pg_no (spte->page) == pgno)
            {
              lock_release (&frame_list_lock);
              return spte;
            }
        }
    }
  lock_release (&frame_list_lock);
  return NULL;
}

/* Returns true if spte->page is writable */
bool is_writable (struct spte *spte)
{
  return (bool)spte->src4;
}

/* Removes spte from thread's Supplemental Page Table */
void
remove_spte (struct spte *spte)
{
  ASSERT (spte != NULL);
  lock_acquire (&frame_list_lock);
  if (spte->frame != NULL)
    remove_from_frame (spte, spte->src_type == PGSRC_FILE);
  lock_release (&frame_list_lock);
  
  lock_acquire (&spte->owner->spt_lock);
  //list_remove (&spte->elem);
  hash_delete (&spte->owner->spt, &spte->elemh);
  lock_release (&spte->owner->spt_lock);
  free_mem_malloc (spte);
  return;
}

/* clear function to be used in hash_clear () */
void spte_hash_clear_func (struct hash_elem *, void *);
void spte_hash_clear_func (struct hash_elem *e, void *aux UNUSED)
{
  struct spte *spte = hash_entry (e, struct spte, elemh);
  free_mem_malloc (spte);
  return;
}

/* Removes all spte for running thread, including the ones currently present
   in frame_list */
void remove_all_spte ()
{
  struct list_elem *e;
  struct thread *t = thread_current ();
  lock_acquire (&frame_list_lock);
  for (e = list_begin (&frame_list); e != list_end (&frame_list);)
    {
      struct spte *spte = list_entry (e, struct spte, elem);
       e = list_next (e);
      if (spte->owner == t)  
          remove_from_frame (spte, spte->src_type == PGSRC_FILE);
    }
  lock_release (&frame_list_lock);
  
  lock_acquire (&t->spt_lock);
  
  hash_clear (&t->spt, spte_hash_clear_func);
 
  /*for (e = list_begin (&t->spt); e != list_end (&t->spt);)
    {
      struct spte *spte = list_entry (e, struct spte, elem); 
      e = list_remove (e);
      free_mem_malloc (spte);
    }*/
  lock_release (&t->spt_lock);
  
  return;
}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
bool
install_page (void *upage, void *kpage, bool writable)
{
  struct thread *t = thread_current ();

  /* Verify that there's not already a page at that virtual
     address, then map our page there. */
  return (pagedir_get_page (t->pagedir, upage) == NULL
          && pagedir_set_page (t->pagedir, upage, kpage, writable));
}

/* Obtains a new frame.
   If no free frame is available, evict a frame using Second-chance page
   replacement algorithm  */
void* get_frame () 
{
  void *frame = palloc_get_page (PAL_USER | PAL_ZERO);
  if (frame == NULL) 
    { 
      lock_acquire (&frame_list_lock);
      struct spte *spte;
      /* Second-chance Page Replacement algorithm */
      while (1)
        {
          spte = list_entry (list_begin (&frame_list), struct spte, elem);
          if (pagedir_is_accessed (spte->owner->pagedir, spte->page))
            {
              pagedir_set_accessed (spte->owner->pagedir, spte->page, false);
              list_remove (&spte->elem);
              list_push_back (&frame_list, &spte->elem);
            }
          else
            {
              break;
            }
          
        }
      remove_from_frame (spte, true);
      frame = palloc_get_page (PAL_USER | PAL_ZERO);
      lock_release (&frame_list_lock);
    }
  ASSERT (frame != NULL);
  return frame;
}
