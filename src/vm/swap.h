#ifndef VM_SWAP_H
#define VM_SWAP_H

#include "lib/kernel/list.h"
#include "devices/block.h"
#include "threads/thread.h"

block_sector_t write_frame_to_swap (void*, struct thread*);
void read_page_from_swap (block_sector_t, void*);
void remove_from_swap (block_sector_t);
void thread_clean_swap (void);
void swap_init (void);
#endif /* vm/swap.h */
